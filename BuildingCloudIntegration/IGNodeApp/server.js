//Grap the packages/variables needed

var express = require("express");
var app = express();
var ig = require("instagram-node").instagram();

var PORTNUMBER = 8080;

//Configure app

//Tell Node where to look for site resources
app.use(express.static(__dirname + "/public"));

//Set the view engine to ejs
app.set("view engine", "ejs");

//Configure instagram app with your access token
ig.use({
    access_token : '6743022473.1677ed0.d23da667cf4c4ff9b151d36f26f01e0a'
});

//SET THE ROUTES
//========================
app.get("/", function (req, res) {
    //Use the Instagram package to get our profile's media
    ig.user_self_media_recent(function(err, medias, pegination, remaining, limit){

    //Render the home pahe and pass in our profile's images
    res.render("pages/index", {grams: medias});
    });
});

//START THE SERVER
//=================

app.listen(PORTNUMBER);
console.log('App started! Look at http://localhost:' + PORTNUMBER);