console.log("Wow! A tiny Node app!");

//Grap express
var express = require("express");

//Create an Express app
var app = express();

//Create an express route fo the home page
app.get('/', function(req, res) {
    res.sendFile(__dirname + '/index.html');
});

//Start the server on port 8080
app.listen(8080);

//Send a message
console.log("Server has started!");