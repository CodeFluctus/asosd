package fi.example.app.flickrviewer;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.hololo.tutorial.library.Step;
import com.hololo.tutorial.library.TutorialActivity;

public class IntroActivity extends TutorialActivity {

    private static final String MY_PREFERENCES = "MyPreferences";
    private static final String INTRODONE_KEY = "IntroKey";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addFragment(new Step.Builder().setTitle("Wellcome to use FlickrViewer")
                .setContent("With this application you can search images from Flickr")
                .setBackgroundColor(Color.LTGRAY) // int background color
                .setDrawable(R.drawable.flickr_icon) // int top drawable
                .setSummary("Click done to start using application")
                .build());

        addFragment(new Step.Builder().setTitle("Wellcome to use FlickrViewer")
                .setContent("With this application you can search images from Flickr")
                .setBackgroundColor(Color.CYAN) // int background color
                .setDrawable(R.drawable.flickr_icon) // int top drawable
                .setSummary("Click done to start using application")
                .build());
    }

    @Override
    public void finishTutorial() {
        super.finishTutorial();
        SharedPreferences sharedPreferences = getSharedPreferences(MY_PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor spEditor = sharedPreferences.edit();
        spEditor.putBoolean(INTRODONE_KEY, true);
        spEditor.commit();
    }

    @Override
    public void currentFragmentPosition( int position){

    }
}

        /*
        // Note here that we DO NOT use setContentView();

        // Add your slide fragments here.
        // AppIntro will automatically generate the dots indicator and buttons.
        /*addSlide(firstFragment);
        addSlide(secondFragment);
        addSlide(thirdFragment);
        addSlide(fourthFragment);


        // Instead of fragments, you can also use our default slide.
        // Just create a `SliderPage` and provide title, description, background and image.
        // AppIntro will do the rest.

        SliderPage sliderPage = new SliderPage();
        sliderPage.setTitle("Introduction");
        sliderPage.setDescription("This application will search images from Flickr to you!");
        sliderPage.setImageDrawable(R.drawable.flickr_icon);
        sliderPage.setBgColor(Color.WHITE);
        addSlide(AppIntroFragment.newInstance(sliderPage));

        SliderPage sliderPage2 = new SliderPage();
        sliderPage.setTitle("Introduction");
        sliderPage.setDescription("This application will search images from Flickr to you!");
        sliderPage.setImageDrawable(R.drawable.flickr_icon);
        sliderPage.setBgColor(Color.WHITE);
        addSlide(AppIntroFragment.newInstance(sliderPage2));




        //addSlide(AppIntroFragment.newInstance(sliderPage2));


        // OPTIONAL METHODS
        // Override bar/separator color.
        setBarColor(Color.parseColor("#3F51B5"));
        setSeparatorColor(Color.parseColor("#2196F3"));

        // Hide Skip/Done button.
        showSkipButton(true);
        //setButtonsEnabled(true);

        // Turn vibration on and set intensity.
        // NOTE: you will probably need to ask VIBRATE permission in Manifest.
        setVibrate(true);
        setVibrateIntensity(30);

    }

    @Override
    public void onSkipPressed(Fragment currentFragment) {
        super.onSkipPressed(currentFragment);
        // Do something when users tap on Skip button.
    }

    @Override
    public void onDonePressed(Fragment currentFragment) {
        super.onDonePressed(currentFragment);
        SharedPreferences sharedPreferences = getSharedPreferences(MY_PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor spEditor = sharedPreferences.edit();
        spEditor.putBoolean(INTRODONE_KEY, true);
        spEditor.commit();
        // Do something when users tap on Done button.
    }

    @Override
    public void onSlideChanged(@Nullable Fragment oldFragment, @Nullable Fragment newFragment) {
        super.onSlideChanged(oldFragment, newFragment);
        // Do something when the slide changes.
    }


}*/
