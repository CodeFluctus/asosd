package fi.example.app.flickrviewer;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class VolleyImageSearcher extends Volley {

    private VISInterface callback = null;

    interface VISInterface
    {
        void allImageUrlsGathered(ArrayList<String> allImageUrls);
    }

    public void setCallback(VISInterface callback) {
        this.callback = callback;
    }

    public void getImageUrlsFor(String imageTag, Context c)
    {
        // Instantiate the RequestQueue.
        RequestQueue queue = Volley.newRequestQueue(c);
        String url ="https://api.flickr.com/services/feeds/photos_public.gne?nojsoncallback=?&format=json&tags=" + imageTag;

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the first 500 characters of the response string.
                        //Log.d("ASD", "Response is: "+ response);
                        getEveryImagesUrlLinks(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("ASD","That didn't work!");
            }
        });

        queue.add(stringRequest);
    }

    private void getEveryImagesUrlLinks(String fromThisUrl)
    {
        ArrayList<String> allImageUrlLinks = new ArrayList<>();

        /* JSON's structure
         * {
         *   ...,
         *   "items" : [
         *       {
         *           ...,
         *           "media" : {"m" : "https://linkToImageWhatSearching.com"},
         *       }
         *       repeats n times
         *    ]
         * }
         * */
        try
        {
            JSONObject mainObj = new JSONObject(fromThisUrl);
            JSONArray itemsArray = mainObj.getJSONArray("items");
            Log.d("JSON", "itemsArray.length = " + itemsArray.length());
            for (int i = 0; i < itemsArray.length(); i++) {
                JSONObject singleItemObj = itemsArray.getJSONObject(i);
                //Log.d("JSON", "SingleObj: " + singleItemObj.toString());
                JSONObject media_m = singleItemObj.getJSONObject("media");
                String linkToSingleImage = media_m.getString("m");
                //Log.d("JSON", linkToSingleImage);
                allImageUrlLinks.add(linkToSingleImage);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        if(callback != null)
        {
            callback.allImageUrlsGathered(allImageUrlLinks);
        }

    }
}
