package fi.example.app.flickrviewer;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ActionBar;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.media.Image;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.HashMap;

import static fi.example.app.flickrviewer.R.drawable.ic_launcher_background;
import static fi.example.app.flickrviewer.R.id.singleImageIV;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, VolleyImageSearcher.VISInterface {

    private static final String MY_PREFERENCES = "MyPreferences";
    private static final String INTRODONE_KEY = "IntroKey";
    private EditText etSearch;
    private VolleyImageSearcher volleyImageSearcher;
    //private ListView listViewForImages;
    private LinearLayout mainLinearLayout, imageViewsLinearLayout;
    private Context c = MainActivity.this;
    private HashMap<Integer, Integer> hashMapOfImageViews = new HashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        volleyImageSearcher = new VolleyImageSearcher();
        volleyImageSearcher.setCallback(this);

        SharedPreferences sharedPreferences = getSharedPreferences(MY_PREFERENCES, Context.MODE_PRIVATE);

        //On application startup there is not yet any sharedpreferences!
        if(!sharedPreferences.getBoolean(INTRODONE_KEY, false))
        {
            Intent introIntent = new Intent(this, IntroActivity.class);
            startActivity(introIntent);
        }

        etSearch = findViewById(R.id.etSearch);
        mainLinearLayout = findViewById(R.id.mainLinearLayout);


        imageViewsLinearLayout = findViewById(R.id.ImageViewsLinearLayout);
        findViewById(R.id.searchBtn).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.searchBtn)
        {
            String tagToSearch = etSearch.getText().toString();
            if(tagToSearch.equals(""))
            {
                tagToSearch = "cat";
            }
            volleyImageSearcher.getImageUrlsFor(tagToSearch, getApplicationContext());
        }

    }

    @Override
    public void allImageUrlsGathered(final ArrayList<String> allImageUrls) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                //Log.d("ASD", "allImageUrls.length: " + allImageUrls.size());
                //Log.d("ASD", "iVLL.childCount: " + imageViewsLinearLayout.getChildCount());
                for (int i = 0; i < allImageUrls.size(); i++)
                {
                    //Break out of if get more images than there is imageviews reserved.
                    if(imageViewsLinearLayout.getChildCount() == i)
                    {
                        break;
                    }
                    //imageViewsLinearLayout have same ammount
                    Log.d("ASD", "ChildId: "+ imageViewsLinearLayout.getChildAt(i).getId());
                    Log.d("ASD", "URL: "+ allImageUrls.get(i));
                    ImageView imageView = findViewById(imageViewsLinearLayout.getChildAt(i).getId());
                    Glide.with(c).load(allImageUrls.get(i)).into(imageView);
                }
            }
        });
    }


}
