package fi.example.app.kotkanpoikalunch;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;

public class VolleyGetter extends Volley {

    private Context context = null;

    public VolleyGetter(Context c)
    {
        context = c;
    }

    interface VolleyGetterInterface
    {
        void lunchListGathered();
    }

    public void getTodaysLucnhList()
    {
        RequestQueue queue = Volley.newRequestQueue(context);
        String dateString = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        Log.d("ASD", "Date: " + dateString);
        String url ="https://www.amica.fi/api/restaurant/menu/day?date=" + dateString + "&language=fi&restaurantPageId=66287";

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the first 500 characters of the response string.
                        parseLunchList(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("ASD", "That didn't work!");
            }
        });

        // Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    private void parseLunchList(String fullLunchList)
    {
        Log.d("ASD", fullLunchList);
        try
        {
            JSONObject mainObj = new JSONObject(fullLunchList);
            JSONArray SetMenusArray = mainObj.getJSONArray("SetMenus");
            for (int i = 0; i < SetMenusArray.length(); i++) {
                LunchMeal oneLunchMeal = new LunchMeal();
                JSONObject setMenusObj = SetMenusArray.getJSONObject(i);

                oneLunchMeal.setType(setMenusObj.getString("Name"));

            }

            oneLunchMeal.setType(SetMenus.getString("Name"));

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }
}
