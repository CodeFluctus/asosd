package fi.example.app.kotkanpoikalunch;

import java.util.ArrayList;

public class RestaurantMenu {

    ArrayList<LunchMeal> allLunchMeals = new ArrayList<>();

    private static RestaurantMenu singleton = null;
    private RestaurantMenu()
    {

    }

    public static RestaurantMenu getInstance()
    {
        if(singleton != null)
        {
            singleton = new RestaurantMenu();
        }
        return singleton;
    }

    public void addLunchToMenu(LunchMeal lunchMeal)
    {
        allLunchMeals.add(lunchMeal);
    }

    public LunchMeal giveLuchMealFromIndex(int i)
    {
        if(i >= 0 && i < allLunchMeals.size())
        {
            return allLunchMeals.get(i);
        }
        else
        {
            return null;
        }
    }
}
