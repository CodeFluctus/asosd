package fi.example.app.kotkanpoikalunch;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    VolleyGetter volleyGetter;// = new VolleyGetter(getApplicationContext());

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        volleyGetter = new VolleyGetter(getApplicationContext());

        volleyGetter.getTodaysLucnhList();
    }
}
