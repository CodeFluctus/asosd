package fi.example.app.kotkanpoikalunch;

import java.util.ArrayList;
import java.util.HashMap;

public class LunchMeal {
    private String type;

    private ArrayList<HashMap<String , String>> mealParts;
    private ArrayList<String> diets;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public ArrayList<String> getMealParts() {
        return mealParts;
    }

    public void setMealParts(ArrayList<String> mealParts) {
        this.mealParts = mealParts;
    }

    public ArrayList<String> getDiets() {
        return diets;
    }

    public void setDiets(ArrayList<String> diets) {
        this.diets = diets;
    }

    public void addMealParts(String meal)
    {
        mealParts.add(meal);
    }

    public void addDiets(String diet)
    {
        diets.add(diet);
    }
}
