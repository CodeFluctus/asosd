package com.example.pialgorithm;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, PiAlgorithm.PiAlgorithmInterface {

    TextView piTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findViewById(R.id.startButton).setOnClickListener(this);
        piTextView = findViewById(R.id.piTextView);
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.startButton)
        {
            Log.d("ASD", "Button click!");
            PiAlgorithm piAlgorithm = new PiAlgorithm();
            piAlgorithm.setCallback(this);
            piAlgorithm.start();
        }
    }

    private void showPi()
    {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                double pi = Math.PI;
            }
        });
    }

    @Override
    public void informCalculatedPi(final String pi) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                piTextView.setText(pi);
            }
        });
    }
}
