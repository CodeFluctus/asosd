package com.example.pialgorithm;

import android.util.Log;

public class PiAlgorithm extends Thread {

    private long length = 0;
    //private double calculatedPi = 0;
    private String piString;
    private PiAlgorithmInterface callback = null;

    interface PiAlgorithmInterface
    {
        void informCalculatedPi(String pi);
    }

    public void setCallback(PiAlgorithmInterface callback) {
        this.callback = callback;
    }

    @Override
    public void run() {
        super.run();

        givePiByAccuracyOfNumbers(5);


        //Pi-number mysteries!

        /*double tens= 4.0/(20.0*21.0*22.0);
        double hundreds= 4.0/(200.0*201.0*202.0);
        double thousands= 4.0/(2000.0*2001.0*2002.0);
        double tenThousands= 4.0/(20000.0*20001.0*20002.0);
        double hundredThousands= 4.0/(200000.0*200001.0*200002.0);
        double milion = 4/(2000000.0*2000001.0*2000002.0);
        double tenMilion = 4/(20000000.0*20000001.0*20000002.0);
        Log.d("ASD","Tenth iteration: " + tens);
        Log.d("ASD","100th iteration: " + hundreds);
        Log.d("ASD","1000th iteration: " + thousands);
        Log.d("ASD","10,000th iteration: " + tenThousands);
        Log.d("ASD", "100,000th iteration: " + hundredThousands);
        Log.d("ASD", "1,000,000th iteration: " + milion);
        Log.d("ASD", "10,000,000th iteration: " + tenMilion);*/

//
//
//        Log.d("ASD", "START RUNNING");
//        //Using Nilakantha series to calculate Pi
//        for (long i = 0; i < 100; i++)
//        {
//            if(i == 0)
//            {
//                calculatedPi +=3;
//            }
//            else
//            {
//                double basenumber = ((double)i)*2;
//                //Log.d("ASD", "run(basenumber): " + basenumber);
//                double nextNumber = 4/(basenumber * (basenumber+1) * (basenumber+2));
//                //Log.d("ASD", "run(nextNumber): " + nextNumber);
//                //String nextNum = String.format("%.1f", nextNumber);
//                //Log.d("ASD", String.valueOf(nextNum.charAt(2)));
//                double odd = i % 2;
//                if(odd > 0)
//                {
//                    //Log.d("ASD", "+ : " + nextNumber);
//                    calculatedPi += nextNumber;
//                }
//                else
//                {
//                    //Log.d("ASD", "- : " + nextNumber);
//                    calculatedPi -= nextNumber;
//                }
//
//                //Log.d("ASD", "CalculatedPi = " +calculatedPi);
//                //pi = String.format("%1.100", calculatedPi);
//
//                //Log.d("ASD", "-------------");
//                double tenTimes = i % 20;
//                /*if( i > 20 && tenTimes > 0)
//                {
//                    String partOfPi = String.valueOf(calculatedPi);
//                    piString += partOfPi;
//                    Log.d("ASD","calculatedPi: " + calculatedPi);
//
//                    int intConversionOfPi = (int)calculatedPi;
//                    Log.d("ASD","Int conversion from calculatedPi: " + intConversionOfPi);
//
//
//                    Log.d("ASD","PiString so far: " + piString);
//                }*/
//
//            }
//        }
//        //double x = 0.000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000004524134;
//        //Log.d("ASD", String.valueOf(x));
        Log.d("ASD", "CalculatedPi = " + piString);
        Log.d("ASD", "RUNNING ENDED");
        if(this.callback != null)
        {
            callback.informCalculatedPi(piString);
        }
    }

    private void givePiByAccuracyOfNumbers(int accurateNumbersCount)
    {
        /*if(wantedCount < 3)
        {
            wantedCount = 3;
        }*/
        double calculatedPi = 0;
        calculatedPi +=3;
        double multiplier = 1;
        piString = "" + (int)calculatedPi + ".";
        int nextMultiplierIncreaseAt = 10;

        if(accurateNumbersCount > 1)
        {
            int iterations = (int)(0.01 * Math.pow(10,accurateNumbersCount));
            Log.d("ASD", "iterates " + iterations + " times!");
            for (int i = 1; i < iterations+1; i++) {
                /*if(i == 0)
                {
                    calculatedPi +=3;
                }
                else
                {*/

                double basenumber = ((double)i)*2;
                double nextNumber = (4/(basenumber * (basenumber+1) * (basenumber+2))*multiplier);


                double odd = i % 2;
                if(odd > 0)
                {
                    Log.d("ASD", "+ : " + nextNumber);
                    calculatedPi += nextNumber;
                }
                else
                {
                    Log.d("ASD", "- : " + nextNumber);
                    calculatedPi -= nextNumber;
                }

                //}

                if(i == nextMultiplierIncreaseAt)
                {
                    Log.d("ASD", "" + calculatedPi);
                    String partOfPi = String.format("%." + accurateNumbersCount + "f", calculatedPi);
                    partOfPi = partOfPi.substring(2, partOfPi.length()-1);
                    Log.d("ASD", "Appending piString: "+ piString + " + " +partOfPi);
                    piString +=partOfPi;


                    multiplier *=1000;
                    if(nextMultiplierIncreaseAt == 100)
                    {
                        calculatedPi -= 3.0;
                    }
                    else
                    {
                        calculatedPi *= multiplier;
                    }
                    Log.d("ASD", "calculatedPi: " + calculatedPi);

                    Log.d("ASD", "Continuing counting from: " + calculatedPi);
                    nextMultiplierIncreaseAt *=10;
                    Log.d("ASD", "---------------------");
                }
            }

        }
    }

}
