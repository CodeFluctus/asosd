package com.example.androidannotations2;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.Layout;
import android.util.Log;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.androidannotations2.R;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

@EActivity (R.layout.activity_main)

public class MainActivity extends AppCompatActivity {

    private static int color = R.color.WHITE;
    private String gameStatus = "IDLE";
    private boolean reactionTimerAlive = true;

    @ViewById
    Button gameButton;

    @ViewById
    TextView reactionTextView;

    @ViewById
    LinearLayout mainLinearLayout;

    @Click(R.id.gameButton)
    void buttonPressed()
    {
        //gameStatus is used to control program flow in this method
        if(gameStatus.equals("IDLE"))
        {
            gameStatus = "RUNNING";
            changeBackgroundColor(R.color.WHITE);

            //Start both timers
            startGameTimer();
            getReactionTime();
        }
        else if (gameStatus.equals("RUNNING"))
        {
            //Will react only if background is RED
            if(color == R.color.RED)
            {
                gameStatus = "ENDED";
                //Stops reactionTimer
                reactionTimerAlive = false;
                gameButton.setText("Restart");
            }
        }
        else if(gameStatus.equals("ENDED"))
        {
            gameStatus = "IDLE";
            changeBackgroundColor(R.color.WHITE);
            reactionTimerAlive = true;
            gameButton.setText("Start");
        }

    }

    //Changes background color to red at random time
    @Background
    void startGameTimer()
    {
        double rand = Math.random();
        long timeToSleep = (long)(10000* rand);
        gameButton.setText("STOP");

        Log.d("ASD", "rand: " + rand + " | tts: " + timeToSleep);
        try
        {
            //Sleeps random time between [10 - 0] s
            Thread.sleep((long)(10000*Math.random()));
        }
        catch (Exception e)
        {

        }
        changeBackgroundColor(R.color.RED);
    }

    @Background
    void getReactionTime()
    {
        //Log.d("ASD", "Reaction timer started!");
        double timeToReact = 0;
        int ms = 1;
        //Log.d("ASD", String.valueOf((long)ms));

        //Wait here until background color changes to RED
        while (color != R.color.RED)
        {
            //For some reason needs something to Log. Otherwice at 2. time will get stuck in here?
            Log.d("ASD",".");
        }
        Log.d("ASD", "ESCAPED!");
        //Runs until user presses button after background changed to RED
        while(reactionTimerAlive)
        {

            try{
                Thread.sleep((long)ms);
                timeToReact += ms;
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
        timeToReact /= 1000;

        updateReactionTime(timeToReact);
        //Log.d("ASD", "Reaction timer STOPPED!");
    }


    //Change backgroundColor
    @UiThread
    public void changeBackgroundColor(int colorToChane)
    {
        mainLinearLayout.setBackgroundResource(colorToChane);
        color = colorToChane;
    }

    //Update reaction time to user.
    @UiThread
    public void updateReactionTime(double time)
    {
        Log.d("ASD", "Setting time to " + time);
        reactionTextView.setText(time + " s");
    }


}
