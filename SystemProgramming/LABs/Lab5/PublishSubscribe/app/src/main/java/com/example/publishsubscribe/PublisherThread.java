package com.example.publishsubscribe;

import android.util.Log;

public class PublisherThread extends Thread {
    private String messageToSend;
    private long sleepTime;
    private PublisherThreadInterface observer = null;
    private int repeatTimes;

    public PublisherThread(String messageToBeSent, long timeToSleepBetweenSends, int timesToRepeatSend)
    {
        messageToSend = messageToBeSent;
        sleepTime = timeToSleepBetweenSends;
        repeatTimes = timesToRepeatSend;
    }

    public void setObserver(PublisherThreadInterface observer) {
        this.observer = observer;
    }

    interface PublisherThreadInterface
    {
        void sendMessage(String message);
    }

    @Override
    public void run() {
        super.run();

        for (int i = 0; i < repeatTimes; i++)
        {
            try {
                sleep(sleepTime);
                if((i+1) == repeatTimes)
                {
                    messageToSend = "Last!: " +messageToSend;
                }
                observer.sendMessage(messageToSend);
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }

    }
}
