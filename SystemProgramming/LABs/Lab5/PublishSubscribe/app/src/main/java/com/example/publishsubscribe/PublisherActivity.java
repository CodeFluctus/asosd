package com.example.publishsubscribe;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class PublisherActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText etThreadMsg, etThreadSendCount, etThreadTimeBetweenMsgs;
    private ArrayList<PublisherThread> allPublishers = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_publisher);
        findViewById(R.id.createThreadButton).setOnClickListener(this);
        etThreadMsg = findViewById(R.id.threadMessage);
        etThreadSendCount = findViewById(R.id.threadSendCount);
        etThreadTimeBetweenMsgs = findViewById(R.id.threadTimeBetweenMessages);

    }

    @Override
    protected void onResume() {
        super.onResume();
        /*Clear engine, so if user returns more than one time to second activity
        * it won't try start Threads that have already ended.*/

        PublisherThreadEngine.getInstance().clearEngine();
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.createThreadButton)
        {
            //Read user data from activity and make thread using those parameters
            String message = etThreadMsg.getText().toString();
            int count = Integer.parseInt(etThreadSendCount.getText().toString());
            int timeInS = Integer.parseInt(etThreadTimeBetweenMsgs.getText().toString());
            long longTimeInS = (long)(timeInS * 1000);

            PublisherThread publisherThread = new PublisherThread(message,longTimeInS, count);

            allPublishers.add(publisherThread);

            etThreadMsg.setText("");
            etThreadTimeBetweenMsgs.setText("");
            etThreadSendCount.setText("");

            PublisherThreadEngine.getInstance().addPublisherThread(publisherThread);

            Toast.makeText(this,"New thread created!", Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    protected void onPause() {
        super.onPause();
        Log.d("ASD", "OnPause!");
        //Start all user created threads!
        PublisherThreadEngine.getInstance().startAllPublisherThreads();
    }
}
