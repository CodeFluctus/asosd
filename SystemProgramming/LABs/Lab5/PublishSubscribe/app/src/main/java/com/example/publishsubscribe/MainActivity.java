package com.example.publishsubscribe;

import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findViewById(R.id.startSecondActivityBtn).setOnClickListener(this);
        textView = findViewById(R.id.publishTextView);

        Log.d("ASD", "Receiver registered!");
        LocalBroadcastManager.getInstance(this).registerReceiver(messageReceiver, new IntentFilter("THREAD"));
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("ASD", "Receiver registered!");
        LocalBroadcastManager.getInstance(this).registerReceiver(messageReceiver, new IntentFilter("THREAD"));
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("ASD", "MainActivity onStop");
        LocalBroadcastManager.getInstance(this).unregisterReceiver(messageReceiver);
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.startSecondActivityBtn)
        {
            Intent intent = new Intent(this, PublisherActivity.class);
            startActivity(intent);
        }
    }

    private BroadcastReceiver messageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d("ASD", "Recieved!");
            Bundle extras = intent.getExtras();

            textView.append("Message: " + extras.getString("MSG") + "\n");
        }
    };
}