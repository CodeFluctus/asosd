package com.example.publishsubscribe;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import java.util.ArrayList;

public class PublisherThreadEngine implements PublisherThread.PublisherThreadInterface{
    private Context context;
    private static ArrayList<PublisherThread> allPublisherThreads = new ArrayList<>();
    private static PublisherThreadEngine singleton = null;

    public static PublisherThreadEngine getInstance()
    {
        if(singleton == null)
        {
            singleton = new PublisherThreadEngine();
        }
        Log.d("ASD", "PTE getInstance()");
        return singleton;
    }

    private PublisherThreadEngine(){}

    public void clearEngine()
    {
        allPublisherThreads.clear();
    }

    public void addPublisherThread(PublisherThread publisherThread)
    {
        publisherThread.setObserver(this);
        allPublisherThreads.add(publisherThread);
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public void startAllPublisherThreads()
    {
        for (int i = 0; i < allPublisherThreads.size(); i++) {
            allPublisherThreads.get(i).start();
        }
        Log.d("ASD", "All threads started!");
    }

    @Override
    public void sendMessage(String message) {
        Log.d("ASD","Recivied message from thread: " + message);
        reportViaPublishAndSubscribe(message);
    }

    private void reportViaPublishAndSubscribe(String message)
    {
        Intent intent = new Intent("THREAD");
        intent.putExtra("MSG", message);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
        Log.d("ASD","BroadCasted: " + message);
    }
}
