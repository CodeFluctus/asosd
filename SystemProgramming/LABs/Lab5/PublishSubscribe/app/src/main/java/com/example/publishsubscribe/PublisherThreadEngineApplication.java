package com.example.publishsubscribe;

import android.app.Application;
import android.util.Log;

public class PublisherThreadEngineApplication extends Application {

    private PublisherThreadEngine publisherThreadEngine = PublisherThreadEngine.getInstance();

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d("ASD", "PTEApplication!");
        publisherThreadEngine.setContext(this);
    }
}
