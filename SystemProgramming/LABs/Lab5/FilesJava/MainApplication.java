import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.InputStreamReader;

/**
 * MainApplication
 */
public class MainApplication {

    public static void main(String[] args) {
        MainApplication application = new MainApplication();
        application.readFileNamed("testForJava.txt");
    }

    private void readFileNamed(String fileNameAtRoot)
    {
        File file = new File(fileNameAtRoot);

        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            System.out.println("\nFile '" + fileNameAtRoot + "' contains text:");
            String dataFromFile;
            while ((dataFromFile = br.readLine()) != null)
            {
                System.out.println(dataFromFile);
            }
            br.close();
        } catch (Exception e) {
            //TODO: handle exception
        }

        writeToEndOfFile(file);
    }

    private void writeToEndOfFile(File file)
    {
        System.out.print("\nPlease enter new text:->");
        try 
        {
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            String userInput = br.readLine();

            System.out.println("\nWriting to file...");
            BufferedWriter bw = new BufferedWriter(new FileWriter(file, true));
            bw.newLine();
            bw.write(userInput);

            //No need to use flush first because close()-method does that before closing self.
            bw.close();

            System.out.println("\nWriting done!");
        } catch (Exception e) {
            //TODO: handle exception
        }
        
    }
}