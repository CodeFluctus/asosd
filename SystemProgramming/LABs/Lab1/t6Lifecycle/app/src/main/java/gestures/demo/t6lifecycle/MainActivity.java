package gestures.demo.t6lifecycle;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, CoolTimer.CoolTimerInterface {


    private TextView textView;
    private EditText editText;
    private Button startButton;
    private CoolTimer coolTimer;
    private int timerStartTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textView = findViewById(R.id.textView);
        startButton = findViewById(R.id.button);
        editText = findViewById(R.id.editText);
        startButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.button)
        {
            coolTimer = new CoolTimer();

            coolTimer.setCallback(this);
            Log.d("ASD", "Nappia painettu!");
            timerStartTime = Integer.parseInt(editText.getText().toString());

            coolTimer.setSecondsLeft(timerStartTime);
            coolTimer.execute();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("ASD", "onResume()");
        textView.setText("onResume- method invoked!");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d("ASD", "onStart()");
        textView.setText("onStart- method invoked!");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d("ASD", "onRestart()");
        textView.setText("onRestart- method invoked!");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("ASD", "onStop()");
        textView.setText("onStop- method invoked!");
    }

    @Override
    public void timerReachedZero() {
        onBackPressed();
    }

    @Override
    public void tickTimerDownOneSecond() {
        String timeString = String.valueOf(timerStartTime);
        String stringToDisplay = "Closing app in " + timeString + " seconds!";
        textView.setText(stringToDisplay);
        timerStartTime--;
    }
}
