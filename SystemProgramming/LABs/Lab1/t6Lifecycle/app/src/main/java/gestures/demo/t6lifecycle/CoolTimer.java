package gestures.demo.t6lifecycle;

import android.os.AsyncTask;
import android.util.Log;

public class CoolTimer extends AsyncTask<Void, Integer, Boolean> {

    private CoolTimerInterface callback = null;
    private Thread thread = new Thread();
    private int secondsLeft = 0;

    interface CoolTimerInterface
    {
        void timerReachedZero();
        void tickTimerDownOneSecond();
    }

    public void setCallback(CoolTimerInterface callback) {
        this.callback = callback;
    }

    public void setSecondsLeft(int seconds)
    {
        secondsLeft = seconds;
    }

    @Override
    protected Boolean doInBackground(Void... voids) {
        for (int i = secondsLeft; i > 0; i--)
        {
            Log.d("ASD", "Closing app in " + i + " seconds!");
            try
            {
                thread.sleep(1000);
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            //Ilmoitetaan, että Threadissa on tapahtunut edistystä. Eli 1s kulunut.
            publishProgress(i);
        }
        return null;
    }

    //Interfacen avulla vähennetään laskurista 1s
    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
        callback.tickTimerDownOneSecond();
    }

    //Suljetaan sovellus Interfacen avulla.
    @Override
    protected void onPostExecute(Boolean aBoolean) {
        super.onPostExecute(aBoolean);
        callback.timerReachedZero();
    }
}
