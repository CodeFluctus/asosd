package com.example.tiisucheering2;

import android.util.Log;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;

public class CheeringAudience extends Thread {

    interface CheeringAudienceInterface
    {
        void AudienceCheers(String cheering);
    }

    private boolean allowCheering = false;
    private CheeringAudienceInterface callback = null;

    public void setCallback(CheeringAudienceInterface callback) {
        this.callback = callback;
    }

    public void stopCheering()
    {
        this.allowCheering = false;
    }

    public void startCheering()
    {
        this.allowCheering = true;
    }

    @Override
    public void run() {
        super.run();

        Log.d("ASD", "STARTED");
        while (true)
        {
            LocalDateTime now = LocalDateTime.now();
            DateTimeFormatter formatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.FULL,FormatStyle.MEDIUM);
            String formattedDate = now.format(formatter);

            if(this.allowCheering)
            {
                Log.d("ASD", formattedDate + ": Tiisu, we want more!");
                callback.AudienceCheers(formattedDate + ": Tiisu, we want more!");
            }

            try
            {
                sleep(5000);
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }

        }
    }
}
