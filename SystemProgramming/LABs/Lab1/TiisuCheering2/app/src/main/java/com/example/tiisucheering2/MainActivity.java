package com.example.tiisucheering2;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements CheeringAudience.CheeringAudienceInterface, View.OnClickListener {

    //Creating new cheeringAudience-object
    private CheeringAudience cheeringAudience = new CheeringAudience();

    private TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findViewById(R.id.buttonStart).setOnClickListener(this);
        findViewById(R.id.buttonStop).setOnClickListener(this);
        textView = findViewById(R.id.textView);

        //Setting callback to object
        cheeringAudience.setCallback(this);
    }

    @Override
    public void onClick(View v) {
        //If user clicks start button audience starts cheering
        if(v.getId() == R.id.buttonStart)
        {
            cheeringAudience.startCheering();
            //Starts thread if thread isn't started already
            if(!cheeringAudience.isAlive())
            {
                cheeringAudience.start();
            }
        }
        //Sets object's boolean to false so audience stops cheering
        else if(v.getId() == R.id.buttonStop)
        {
            cheeringAudience.stopCheering();
        }
    }


    //Implements method from CheeringAudience-object.
    @Override
    public void AudienceCheers(final String cheering) {
        //Must initialize runnable where cheering is updated to UI,
        //because updating is required outside of UI thread.
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                textView.setText(cheering);
            }
        };
        runOnUiThread(runnable);
    }


}
