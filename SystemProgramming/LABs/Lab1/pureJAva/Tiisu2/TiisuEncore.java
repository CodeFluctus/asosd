import java.io.BufferedReader;
import java.io.InputStreamReader;

class TiisuEncore
{
    //private static TiisuEncore tiisuEncore = new TiisuEncore();
    private String userDecision = "null";

    public String getUserDecision() {
        return this.userDecision;
    }
    public void setUserDecision(String userDecision) {
        this.userDecision = userDecision;
    }

    public static void main(String[] args) {
        CheeringAudience cheeringAudience = new CheeringAudience();
        
        TiisuEncore tiisuEncore = new TiisuEncore();

        while (!tiisuEncore.getUserDecision().equals("QUIT")) 
        {
            tiisuEncore.askDecisions();
            if(tiisuEncore.getUserDecision().equals("START"))
            {
                cheeringAudience.start();
                System.out.println("Huuto alkaa");
                cheeringAudience.startCheering();
                
            }
            else if(tiisuEncore.getUserDecision().equals("STOP"))
            {
                System.out.println("Huuto hiljeni!");
                cheeringAudience.stopCheering();
            }
            else if(tiisuEncore.getUserDecision().equals("QUIT"))
            {
                cheeringAudience.stopCheering();
                cheeringAudience.interrupt();
                System.out.println("Ohjelma suljetaan!");
            }
        }
    }

    private void askDecisions()
    {
        System.out.println("Master, give your command:");
        try 
        {
            BufferedReader reader = new BufferedReader(
                new InputStreamReader(System.in));
            String userDecision = reader.readLine().toUpperCase();
            this.setUserDecision(userDecision);
        } 
        catch (Exception e) {
            e.printStackTrace();
        }
        
        

    }
}