/**
 * CheeringAudience
 */

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;

public class CheeringAudience extends Thread {

    private boolean allowCheering = false;

    public void stopCheering()
    {
        this.allowCheering = false;
        System.out.println(this.allowCheering);
    }

    public void startCheering()
    {
        this.allowCheering = true;
        System.out.println(this.allowCheering);
    }

    @Override
    public void run()
    {
        //System.out.println("...");
        while(this.allowCheering)
        {
            LocalDateTime now = LocalDateTime.now();
            DateTimeFormatter formatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.FULL, FormatStyle.MEDIUM);
            String formattedDate = now.format(formatter);

            System.out.println(formattedDate + ": Tiisu, we want more!");
            try 
            {
                sleep(5000);
            } 
            catch (Exception e) 
            {
                e.printStackTrace();
            }
        }
        
    }
    
}