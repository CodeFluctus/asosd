import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * InternetThing
 */
public class InternetThing extends Thread{

    private String urlString;

    public void setUrlString(String urlString) {
        this.urlString = urlString;
    }

    @Override
    public void run() {
        super.run();

        try {
            URL url = new URL(urlString);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            InputStream inputStream = urlConnection.getInputStream();

            String stream = convertInputStreamToString(inputStream);
            System.out.println(stream);

            
        } catch (Exception e) {
            //TODO: handle exception
            e.printStackTrace();
        }
    }

    public static String convertInputStreamToString(InputStream inputStream)
    {
        String stringToReturn = "";
        try{
            /*Luetaan ensimmäinen merkki. Tällä saadaan myös käynnistymään available-metodi
            * Ilman tätä väittää saatavilla olevien merkkien määrän 0:si;
            * Koska read-metodi palauttaa ASCII-koodatun kokonaisluvun muutetaan se Character
            * olion avulla char-merkiksi ja tallennetaan stringiin.*/
            stringToReturn += Character.toString((char)inputStream.read());

            //Luetaan merkkejä Stringiin niin pitkään kuin merkkejä on saatavilla.
            while (inputStream.available() > 0)
            {
                stringToReturn += Character.toString((char)inputStream.read());
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        //Palautetaan luotu stringi.
        return stringToReturn;
    }
}