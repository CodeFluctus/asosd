package com.example.tamagochigame;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity implements View.OnClickListener
{

    private PlayersEngine playersEngine;
    private ListView highScoresListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findViewById(R.id.buttonStartGame).setOnClickListener(this);
        playersEngine = PlayersEngine.getInstance();
        highScoresListView = findViewById(R.id.highScoreListView);

        //Add random stats to engine to proof that sorter for points works correctly.
        playersEngine.addNewPlayer(new Player("Keijo", 2, 12));
        playersEngine.addNewPlayer(new Player("Otto", 4, 14));
        playersEngine.addNewPlayer(new Player("Mauri", 1, 140));
        playersEngine.addNewPlayer(new Player("Sauli", 4, 23));
        playersEngine.addNewPlayer(new Player("Seppo", 1, 90));
        playersEngine.addNewPlayer(new Player("Urmas", 3, 14));
        playersEngine.addNewPlayer(new Player("Anneli", 5, 3));

        playersEngine.orderHighScoresHighToLow();

        updateHighScores();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("ASD", "onResume");
        playersEngine.orderHighScoresHighToLow();
        updateHighScores();
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.buttonStartGame)
        {
            EditText playerNameET = (EditText) findViewById(R.id.playerNameEditText);
            String playerName = playerNameET.getText().toString();

            EditText gameSpeedET = (EditText) findViewById(R.id.gameSpeedEditText);
            String gameSpeedString = gameSpeedET.getText().toString();
            //If user don't input any number force game speed to 1
            if(gameSpeedString.equals(""))
            {
                gameSpeedString = "1";
            }
            int gameSpeed = Integer.parseInt(gameSpeedString);

            //Keep game speed under asked measurements
            if(gameSpeed < 0)
            {
                gameSpeed = 1;
            }
            else if(gameSpeed > 5)
            {
                gameSpeed = 5;
            }
            Log.d("ASD", "PlayerName: " + playerName + ". GameSpeed: " + gameSpeed);

            //Post data with extra
            Intent intent = new Intent(this, GameActivity.class);
            intent.putExtra("PLAYERNAME", playerName);
            intent.putExtra("GAMESPEED", gameSpeed);

            startActivity(intent);

        }
    }

    private void updateHighScores()
    {
        ArrayList<HashMap<String, String>> list = new ArrayList<>();

        for (int i = 0; i < playersEngine.getPlayersEngineSize(); i++) {

            Player player = playersEngine.getPlayerFromIndex(i);
            HashMap<String, String> hashMap = new HashMap<>();

            hashMap.put("Name", player.getName());
            hashMap.put("GS", String.valueOf(player.getGameSpeedUsed()));
            hashMap.put("TS", String.valueOf(player.getTimeSurvived()));

            list.add(hashMap);
        }

        String from[] = {"Name", "GS", "TS"};
        int to[] = {R.id.statsPlayerName, R.id.statsGameSpeedUsed, R.id.statsTimeSurvived};

        SimpleAdapter adapter = new SimpleAdapter(this, list, R.layout.custom_stats_row, from, to);
        highScoresListView.setAdapter(adapter);
    }
}
