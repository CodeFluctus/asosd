package com.example.tamagochigame;

import androidx.annotation.NonNull;

/**
 * OneTamagochi
 */
public class OneTamagochi extends Thread{

    interface OneTamagochisInterface
    {
        void tamagochiFeeded();
    }

    private int foodAmmount = 10;
    private String name = null;
    private String[] namePool = {"Aki", "Ulla", "Ismo", "Kari", "Seppo", "Miia", "Kalle", "Saku", "Anni"};
    private boolean isAlive = true;
    private int foodConsumptionSpeed = 1;
    private OneTamagochisInterface callback = null;

    OneTamagochi()
    {
        double rand = Math.random();
        System.out.println("Rand: " + rand);
        System.out.println("NamePool: " + namePool.length);
        int nro = (int)(rand*(namePool.length));
        this.foodAmmount +=nro;
        this.name = namePool[(int)(Math.random()*(namePool.length))];
        System.out.println(this.name);
    }

    public void setCallback(OneTamagochisInterface callback) {
        this.callback = callback;
    }

    public void setFoodConsumptionSpeed(int foodConsumptionSpeed) {
        this.foodConsumptionSpeed = foodConsumptionSpeed;
    }

    @Override
    public void run() {
        // TODO Auto-generated method stub
        super.run();
        //While thread is alive keep running
        while(this.isAlive)
        {
            try 
            {
                //Consume one food
                this.foodAmmount -=1;
                //If food amount reaches 0. Tamagotchi dies.
                if(this.foodAmmount == 0)
                {
                    this.isAlive = false;
                }

                //With foodConsumptionSpeed is managed how fast tamagochi will eat.
                sleep(2000/foodConsumptionSpeed);
                
            } catch (Exception e) 
            {
                //TODO: handle exception
                e.printStackTrace();
            }
        }
        
    }

    public String getTamagochiName() {
        return this.name;
    }

    public int getFoodAmmount() {
        return foodAmmount;
    }

    public void setTamagochiName(String newName)
    {
        this.name = newName;
    }

    public void feedTamagochi()
    {
        //Feed tamagochi if it is alive.
        if(this.isAlive)
        {
            this.foodAmmount +=10;
            //If tamagochi get too much food it will die.
            if(this.foodAmmount > 20)
            {
                this.isAlive = false;
            }
            //Inform interface that tamagochi got fed.
            callback.tamagochiFeeded();
        }
    }

    public String giveStatus()
    {
        String statusString = null;
        if(this.isAlive)
        {
            //System.out.println("Is happy and alive!");
            statusString = "Is happy and alive!";

        }
        else if(!this.isAlive && this.foodAmmount >= 20)
        {
            //System.out.println("Got too fat and died to obiesity!");
            statusString = "Got too fat and died to obiesity!";
        }
        else
        {
            //System.out.println("Died to hunger!");
            statusString = "Died to hunger!";
        }

        return statusString;
    }

    public boolean isTamagochiAlive()
    {
        return this.isAlive;
    }

    public void killTamagochi()
    {
        this.isAlive = false;
    }


}