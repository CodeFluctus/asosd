package com.example.tamagochigame;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;

public class GameActivity extends AppCompatActivity implements TamagochiEngine.TamagochiEngineInterface, ListView.OnItemClickListener {

    private TamagochiEngine tamagochiEngine;
    private ListView tamagochiGameLV;
    private SimpleAdapter adapter;
    private PlayersEngine playersEngine;
    private Player player;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
        tamagochiGameLV = findViewById(R.id.gameListView);
        tamagochiGameLV.setOnItemClickListener(this);
        playersEngine = PlayersEngine.getInstance();
        player = new Player();

        int gameSpeed = 1;
        if(savedInstanceState == null)
        {
            Bundle extras = getIntent().getExtras();
            if(extras == null)
            {

            }
            else
            {
                gameSpeed = extras.getInt("GAMESPEED");
                String playerName = extras.getString("PLAYERNAME");
                player.setName(playerName);
                player.setGameSpeedUsed(gameSpeed);
            }
        }

        tamagochiEngine = new TamagochiEngine();
        Log.d("ASD", "Game speed set to: " + gameSpeed);
        tamagochiEngine.setGameSpeed(gameSpeed);

        for(int i = 0; i < 7; i++)
        {
            OneTamagochi tamagochi = new OneTamagochi();
            tamagochiEngine.addTamagochi(tamagochi);
        }

        setAdapter();
        tamagochiEngine.setCallback(this);
        tamagochiEngine.start();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Log.d("ASD", "CLICKED: " + position);
        tamagochiEngine.feedTamagochiAtIndex(position);
    }

    @Override
    public void updateStatus() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                setAdapter();
            }
        });
    }

    @Override
    public void gameOverMessage(final int timeSurvived) {
        player.setTimeSurvived(timeSurvived);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getApplicationContext(), "You survived " + timeSurvived + " seconds! Press back to play again", Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        playersEngine.addNewPlayer(player);

        tamagochiEngine.burnThemAll();
        tamagochiEngine.clearEngine();
    }

    private void setAdapter()
    {
        ArrayList<HashMap<String,String>> arrayList = new ArrayList<>();

        for(int i = 0; i < tamagochiEngine.getEngineSize(); i++)
        {
            OneTamagochi tamagochi = tamagochiEngine.giveTamagotchiFromIndex(i);
            HashMap<String, String> hashMap = new HashMap<>();
            hashMap.put("FoodAmmount", Integer.toString(tamagochi.getFoodAmmount()));
            hashMap.put("TamagochiName", tamagochi.getTamagochiName());
            hashMap.put("TamagochiStatus", tamagochi.giveStatus());
            arrayList.add(hashMap);
        }

        String from[] = {"FoodAmmount", "TamagochiName", "TamagochiStatus"};
        int to[] = { R.id.foodAmmount, R.id.tamagotchiName, R.id.tamagotchiStatus};

        adapter = new SimpleAdapter(this, arrayList, R.layout.custom_row, from, to);
        tamagochiGameLV.setAdapter(adapter);
    }
}

    /*@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
    }
}*/
