package com.example.tamagochigame; /**
 * TamagochiEngine
 */

 import java.util.ArrayList;

public class TamagochiEngine extends Thread  implements OneTamagochi.OneTamagochisInterface {

    private ArrayList<OneTamagochi> allTamagochis = new ArrayList<>();
    private boolean isGameOver = false;
    private int gameClock = 0;
    private int gameSpeed = 1;
    private TamagochiEngineInterface callback = null;

    interface TamagochiEngineInterface
    {
        void updateStatus();
        //Inform user about how long he/she survived.
        void gameOverMessage(final int timeSurvived);
    }

    public void setCallback(TamagochiEngineInterface callback) {
        this.callback = callback;
    }

    public void addTamagochi(OneTamagochi oneTamagochi)
    {
        oneTamagochi.setFoodConsumptionSpeed(gameSpeed);
        oneTamagochi.setCallback(this);
        this.allTamagochis.add(oneTamagochi);
    }

    public void setGameSpeed(int gameSpeed) {
        this.gameSpeed = gameSpeed;
    }

    public void printAllTamagochisStatus()
    {
        System.out.println();
        System.out.println();
        System.out.println("Tamagotchi statuses:");
        int i = 1;
        for (OneTamagochi oneTamagochi : allTamagochis) {
            System.out.print(i+". ");
            //oneTamagochi.printStatus();
            i++;
        }
    }

    public boolean isGameEnded()
    {
        if(this.isGameOver)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    @Override
    public void tamagochiFeeded() {
        callback.updateStatus();
    }

    public int getEngineSize()
    {
        return allTamagochis.size();
    }

    public OneTamagochi giveTamagotchiFromIndex(int i)
    {
        return allTamagochis.get(i);
    }

    @Override
    public void run() {
        // TODO Auto-generated method stub
        super.run();
        startDeathMarch();
        while(!isGameOver)
        {
            if(!isNeededCountAlive(2))
            {
                this.isGameOver = true;
            }
            try {
                //printAllTamagochisStatus();
                gameClock++;
                sleep(1000);
                
            } catch (Exception e) {
                //TODO: handle exception
                e.printStackTrace();
            }
            callback.updateStatus();
        }
        burnThemAll();
        clearEngine();
        System.out.println();
        String endGameMsg = "Game ended! You survived " + gameClock + " seconds!";
        System.out.println(endGameMsg);
        callback.gameOverMessage(gameClock);
    }

    public void gameIsOver()
    {
        this.isGameOver = true;
    }

    public void startDeathMarch()
    {
        for (OneTamagochi oneTamagochi : allTamagochis) {
            oneTamagochi.start();
        }
    }

    public boolean isEveryoneAlive()
    {
        for (OneTamagochi oneTamagochi : allTamagochis) {
            if(!oneTamagochi.isTamagochiAlive())
            {
                return false;
            }
        }
        return true;
    }

    public void feedTamagochiAtIndex(int number)
    {
        int i = number;// for pure java use -1;
        if(i >= 0 && i < allTamagochis.size())
        {
            allTamagochis.get(i).feedTamagochi();
        }
        else
        {
            System.out.println("No such Tamagochi number: " + number);
        }
        
    }

    public boolean isNeededCountAlive(int count)
    {
        int deadOnes = 0;
        for (OneTamagochi oneTamagochi : allTamagochis) {
            if(!oneTamagochi.isTamagochiAlive())
            {
                deadOnes +=1;
                if(deadOnes >= count)
                {
                    return false;
                }
            }
        }
        return true;
    }



    public void clearEngine()
    {
        allTamagochis.clear();
    }

    public void burnThemAll()
    {
        for (OneTamagochi oneTamagochi : allTamagochis) {
            oneTamagochi.killTamagochi();
        }
    }
}