package com.example.tamagochigame;

public class Player {
    private String name;
    private int gameSpeedUsed;
    private int timeSurvived;

    public Player()
    {

    }

    public Player(String name, int gameSpeedUsed, int timeSurvived)
    {
        this.name = name;
        this.gameSpeedUsed = gameSpeedUsed;
        this.timeSurvived = timeSurvived;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getGameSpeedUsed() {
        return gameSpeedUsed;
    }

    public void setGameSpeedUsed(int gameSpeedUsed) {
        this.gameSpeedUsed = gameSpeedUsed;
    }

    public int getTimeSurvived() {
        return timeSurvived;
    }

    public void setTimeSurvived(int timeSurvived) {
        this.timeSurvived = timeSurvived;
    }
}
