package com.example.tamagochigame;

import android.util.Log;
import android.widget.ArrayAdapter;

import java.util.ArrayList;

public class PlayersEngine {

    private PlayersEngine()
    {

    }

    private static PlayersEngine singleton = null;

    private ArrayList<Player> allPlayers = new ArrayList<>();

    public static PlayersEngine getInstance()
    {
        if(singleton == null)
        {
            singleton = new PlayersEngine();
        }
        return singleton;
    }

    public void addNewPlayer(Player player)
    {
        boolean playerHaveNameOnHighScores = false;
        for (int i = 0; i < allPlayers.size(); i++)
        {
            //If player already have name on High Scores
            if(allPlayers.get(i).getName().equals(player.getName()))
            {
                playerHaveNameOnHighScores = true;
                //Player used faster game speed than last time. Update player high score
                if(allPlayers.get(i).getGameSpeedUsed() < player.getGameSpeedUsed())
                {
                    updatePlayerStatsAtIndex(i, player);
                    break;
                }
                //Already played with same speed
                else if(allPlayers.get(i).getGameSpeedUsed() == player.getGameSpeedUsed())
                {
                    //If player scored new high score. Update stats.
                    if (allPlayers.get(i).getTimeSurvived() < player.getTimeSurvived())
                    {
                        updatePlayerStatsAtIndex(i, player);
                        break;
                    }
                }
            }
            /*else
            {
                allPlayers.add(player);
            }*/
        }
        if(!playerHaveNameOnHighScores)
        {
            allPlayers.add(player);
        }
    }

    public Player getPlayerFromIndex(int index)
    {
        if(index >= 0 && index < allPlayers.size())
        {
            return allPlayers.get(index);
        }
        else
        {
            return null;
        }
    }

    public int getPlayersEngineSize()
    {
        return allPlayers.size();
    }

    public void orderHighScoresHighToLow()
    {
        ArrayList<Player> objHighScores = new ArrayList<>();

        int orderHighToLow[] = {5, 4, 3, 2, 1};
        for (int i = 0; i < orderHighToLow.length; i++) {
            Log.d("ASD", "partList count: " + (i+1));
            ArrayList<Player> partList = getPlayersOrdered(orderHighToLow[i]);
            for (int j = 0; j < partList.size(); j++) {
                objHighScores.add(partList.get(j));
            }
        }
        allPlayers = objHighScores;
    }

    private ArrayList<Player> getPlayersOrdered(int num)
    {
        ArrayList<Player> requiredList = new ArrayList<>();
        for (int i = 0; i < allPlayers.size(); i++) {
            Log.d("ASD", "requiredList.size(): " + requiredList.size());
            if(allPlayers.get(i).getGameSpeedUsed() == num)
            {
                if(requiredList.size() > 0)
                {
                    if(requiredList.get(requiredList.size()-1).getTimeSurvived() < allPlayers.get(i).getTimeSurvived())
                    {
                        Log.d("ASD", "Pushed " + allPlayers.get(i).getName() + " before " + requiredList.get(requiredList.size()-1).getName());
                        requiredList.add(requiredList.size()-1, allPlayers.get(i));
                    }
                    else
                    {
                        Log.d("ASD", "Pushed " + allPlayers.get(i).getName() + " AFTER " + requiredList.get(requiredList.size()-1).getName());
                        requiredList.add(allPlayers.get(i));
                    }
                }
                else
                {
                    Log.d("ASD", "Added first from set " + num +": " + allPlayers.get(i).getName() );
                    requiredList.add(allPlayers.get(i));
                }
            }
        }
        return requiredList;
    }

    private void updatePlayerStatsAtIndex(int i, Player playerToUpdate)
    {
        allPlayers.remove(i);
        allPlayers.add(i, playerToUpdate);
    }
}
