package gestures.demo.t2filesandroid;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements InternetThing.InternetThingInterface, View.OnClickListener {

private EditText etInput;
private TextView tvOutput;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        etInput = findViewById(R.id.editText);
        tvOutput = findViewById(R.id.textView2);
        findViewById(R.id.button).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.button)
        {
            String userInput = etInput.getText().toString();
            InternetThing internetThing = new InternetThing();
            internetThing.setListener(this);
            internetThing.setUrlString(userInput);

            internetThing.start();
        }
    }

    @Override
    public void fetchingCompleted(final String result) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                tvOutput.setText(result);
            }
        });
    }

    @Override
    public void onError(final String error) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                tvOutput.setText(error);
            }
        });
    }
}
