package com.example.thrandobs;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * CTEngine
 */
public class CTEngine implements CustomThread.CTObserver{

    private final HashMap<String, CTEngineObserver> allObservers = new HashMap<>();
    private ArrayList<CustomThread> allCustomThreads = new ArrayList<>();

    private static CTEngine singleton = null;

    CTEngine(){}

    /**
     * CTEngineObserver
     */
    public interface CTEngineObserver {
        //public void updateProgress(int progressPercent);
        public void updateProgress(String msg);
    }

    public static CTEngine getInstance()
    {
        if(singleton == null)
        {
            singleton = new CTEngine();
        }
        return singleton;
    }

    public void addObserver(final String observable, final CTEngineObserver observer)
    {
        allObservers.put(observable, observer);
    }

    public void removeObserver(CTEngineObserver observerToBeRemoved) {
        while (allObservers.values().remove(observerToBeRemoved));
    }

    public void generateNewThreadAndStartIt(int threadNumber)
    {
        System.out.println("Starting generating");
        CustomThread customThread = new CustomThread(threadNumber, this);
        allCustomThreads.add(customThread);
        customThread.start();
    }


    public void generateNewCustomThreads(int wantedCount)
    {
        System.out.println("Starting generating");
        for (int i = 0; i < wantedCount; i++) {
            CustomThread customThread = new CustomThread(i, this);
            allCustomThreads.add(customThread);
        }
    }

    public void startGeneratedThreads()
    {
        for (int i = 0; i < allCustomThreads.size(); i++) {
                allCustomThreads.get(i).start();
        }
    }

    

    @Override
    public void updateProgress(String msg, int threadNumber) {
        // TODO Auto-generated method stub
        reportToObservers(msg, threadNumber);
    }

    private void reportToObservers(String msg, int threadNuber)
    {
        CTEngineObserver allObserver = allObservers.get("ALL");

        if(allObserver != null)
        {
            allObserver.updateProgress(msg);
        }

        String observableString = String.valueOf(threadNuber);

        //System.out.println("Finding string named: " + observableString);
        
        CTEngineObserver specific = allObservers.get(observableString);

        if(specific != null)
        {
            System.out.println("Finding string named: " + observableString);
            specific.updateProgress(msg);
        }
    }

    /*@Override
    public void updateProgress(int progressPercent, int threadNumber) {
        // TODO Auto-generated method stub
        reportToObservers(progressPercent, threadNumber);
    }*/

    /*private void reportToObservers(int progress, int number)
    {
        CTEngineObserver allObserver = allObservers.get("ALL");

        if(allObserver != null)
        {
            allObserver.updateProgress(progress);
        }

        String observableString = String.valueOf(number);
        
        CTEngineObserver specific = allObservers.get(observableString);

        if(specific != null)
        {
            allObserver.updateProgress(progress);
        }
    }*/
}