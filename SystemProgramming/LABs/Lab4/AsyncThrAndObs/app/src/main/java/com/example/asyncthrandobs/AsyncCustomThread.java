package com.example.asyncthrandobs;

import android.os.AsyncTask;
import android.util.Log;

public class AsyncCustomThread extends AsyncTask<Void, Integer, Boolean> {

    private int threadNumber;
    private String stringToTransfer;
    private AsyncCTInterface callback = null;


    public AsyncCustomThread(int threadNumber, AsyncCTInterface callback)
    {
        this.threadNumber = threadNumber;
        this.callback = callback;
        System.out.println("Thread generated!");
    }

    public interface AsyncCTInterface {
        //void updateProgress(int progressPercent, int threadNumber);
        void updateProgress(String msg, int threadNumber);
    }

    @Override
    protected Boolean doInBackground(Void... voids) {
        Log.d("ASD", "Thread starts running!");
        for (int i = 0; i < 11; i++) {
            //Log.d("ASD", "..");
            try
            {
                Thread.sleep(500);
                String updateString = "Thread " + threadNumber + ": " + (i*10) + "%";
                Log.d("ASD", updateString);
                if(callback != null)
                {
                    Integer a = i;
                    callback.updateProgress(updateString, threadNumber);
                    //observer.updateProgress(i, threadNumber);
                }
            } catch (Exception e) {
                //TODO: handle exception
            }
        }
        return null;
    }

    @Override
    protected void onPostExecute(Boolean aBoolean) {
        super.onPostExecute(aBoolean);
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);

    }
}
