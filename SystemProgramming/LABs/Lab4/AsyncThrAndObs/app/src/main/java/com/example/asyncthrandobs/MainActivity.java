package com.example.asyncthrandobs;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, CTEngine.CTEngineObserver {

    private CTEngine ctEngine;
    private TextView textView;
    private int threadsCount = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.startThreadButton).setOnClickListener(this);
        textView = findViewById(R.id.textView);
        ctEngine = CTEngine.getInstance();
        ctEngine.addObserver("ALL", this);
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.startThreadButton)
        {
            Log.d("ASD", "Button pressed!");
            ctEngine.generateNewThreadAndStartIt(threadsCount);
            String textFromTextView = textView.getText().toString();

            textFromTextView += "\n New thread created with id: " + threadsCount;

            textView.setText(textFromTextView);

            threadsCount++;
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        ctEngine.removeObserver(this);
    }

    @Override
    public void updateProgress(final String msg) {

        //Log.d("ASD", msg);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                String textFromTextView = textView.getText().toString();

                textFromTextView +="\n " + msg;

                textView.setText(textFromTextView);
            }
        });
    }
}
