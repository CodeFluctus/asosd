package com.example.weathergetterincomplete2020;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, WeatherEngine.WeatherDataAvailableInterface {

    WeatherEngine engine = new WeatherEngine(this);

    public static final String MY_PREFERENCES = "MyPreferences";
    public static final String CITY_KEY = "cityKey";

    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findViewById(R.id.button).setOnClickListener(this);
        sharedPreferences = getSharedPreferences(MY_PREFERENCES, Context.MODE_PRIVATE);
        EditText editor = (EditText) findViewById(R.id.editText);
        if(sharedPreferences.contains(CITY_KEY))
        {
            editor.setText(sharedPreferences.getString(CITY_KEY, "Helsinki"));
        }

    }

    @Override
    public void onClick(View v) {
        EditText editor = (EditText) findViewById(R.id.editText);
        engine.getWeatherData(editor.getText().toString());
    }

    protected void updateUI()
    {
        TextView temperatureTextView = (TextView) findViewById(R.id.textView);
        String formatted = String.format(getString(R.string.temp), engine.getTemperature());

        temperatureTextView.setText(formatted);
        ImageView img = (ImageView) findViewById(R.id.imageView);
        Picasso.with(this).load("http://openweathermap.org/img/w/" + engine.getIconId() + ".png").into(img);
    }

    @Override
    public void weatherDataAvailable() {
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                updateUI();
            }
        });
    }

    @Override
    protected void onStop() {
        super.onStop();

        EditText editor = (EditText) findViewById(R.id.editText);
        String userCity = editor.getText().toString();

        SharedPreferences.Editor spEditor = sharedPreferences.edit();
        spEditor.putString(CITY_KEY, userCity);
        spEditor.commit();
    }
}