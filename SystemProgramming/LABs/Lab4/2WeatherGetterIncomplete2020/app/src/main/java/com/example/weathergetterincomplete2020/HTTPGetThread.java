package com.example.weathergetterincomplete2020;

import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;


public class HTTPGetThread extends Thread {

    private String urlString = null;
    private OnRequestDoneInterface callback = null;

    public interface OnRequestDoneInterface {
        void onRequestDone(String data);
    }

    HTTPGetThread(String url, OnRequestDoneInterface obj)
    {
        this.urlString = url;
        this.callback = obj;
    }

    @Override
    public void run() {
        super.run();
        try {
            URL url = new URL(urlString);
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();

            InputStream stream = httpURLConnection.getInputStream();
            String streamString = convertInputStreamToString(stream);

            Log.d("ASD", streamString);

            if(callback!= null)
            {
                callback.onRequestDone(streamString);
            }

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public static String convertInputStreamToString(InputStream inputStream)
    {
        String stringToReturn = "";
        try{
            /*Luetaan ensimmäinen merkki. Tällä saadaan myös käynnistymään available-metodi
             * Ilman tätä väittää saatavilla olevien merkkien määrän 0:si;
             * Koska read-metodi palauttaa ASCII-koodatun kokonaisluvun muutetaan se Character
             * olion avulla char-merkiksi ja tallennetaan stringiin.*/
            stringToReturn += Character.toString((char)inputStream.read());

            //Luetaan merkkejä Stringiin niin pitkään kuin merkkejä on saatavilla.
            while (inputStream.available() > 0)
            {
                stringToReturn += Character.toString((char)inputStream.read());
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        //Palautetaan luotu stringi.
        return stringToReturn;
    }


}
