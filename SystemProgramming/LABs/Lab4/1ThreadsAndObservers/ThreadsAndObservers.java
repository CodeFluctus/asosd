import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * ThreadsAndObservers
 */
public class ThreadsAndObservers implements CTEngine.CTEngineObserver{


    public static void main(String[] args) {
        ThreadsAndObservers threadsAndObservers = new ThreadsAndObservers();
        threadsAndObservers.sayHelloWorld();
        threadsAndObservers.askThreadCount();
    }

    private void sayHelloWorld()
    {
        System.out.println("Hello World!");
    }

    private void askThreadCount()
    {
        System.out.println("Type count of threads to start");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        try {
            int threadCount = Integer.parseInt(reader.readLine());
            startThreads(threadCount);
        } catch (Exception e) {
            //TODO: handle exception
        }
    }

    private void startThreads(int count)
    {
        System.out.println("ASD!");
        CTEngine ctEngine = CTEngine.getInstance();
        ctEngine.addObserver("3", this);

        /*for (int i = 0; i < count; i++) {
            System.out.print(".");*/
            ctEngine.generateNewCustomThreads(count);
        //}
        System.out.println("Generation completed!");
        ctEngine.startGeneratedThreads();
    }

    @Override
    public void updateProgress(String msg) {
        System.out.println(msg);
    }
}