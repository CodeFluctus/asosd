/**
 * CustomThread
 */
public class CustomThread extends Thread{

    private int threadNumber;
    private CTObserver observer = null;
    /**
     * CTObserver
     */
    public CustomThread(int threadNumber, CTObserver observer)
    {
        this.threadNumber = threadNumber;
        this.observer = observer;
        System.out.println("Thread generated!");
    }

    public interface CTObserver {
        //void updateProgress(int progressPercent, int threadNumber);
        void updateProgress(String msg, int threadNuber);
    }

    @Override
    public void run() {
        System.out.println("Thread started!");
        // TODO Auto-generated method stub
        super.run();

        for (int i = 0; i < 11; i++) {
            try 
            {
                sleep(500);
                if(observer != null)
                {
                    String updateString = "Thread " + threadNumber + ": " + (i*10) + "%";
                    observer.updateProgress(updateString, threadNumber);
                    //observer.updateProgress(i, threadNumber);
                }
            } catch (Exception e) {
                //TODO: handle exception
            }
        }
        
    }
}