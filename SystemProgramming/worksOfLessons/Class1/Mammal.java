public class Mammal extends Animal{
    MammalType type;
    
    public Mammal()
    {

    }

    public void setMammalType(MammalType mammalType)
    {
        this.type = mammalType;
    }

    public MammalType getType()
    {
        return this.type;
    }


}