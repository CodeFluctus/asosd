public class Animal {

    protected  Animal()
    {

    }

    private int number;
    private String name;
    
    public void setNumber(int num)
    {
        if(num < 1)
        {
            System.out.println("Invalid number!");
        }
        else
        {
            this.number = num;
        }
    }

    public int getNumber()
    {
        return this.number;
    }

    public void setName(String newName)
    {
        this.name = newName;
    }
    
    public String getName()
    {
        return this.name;
    }

    public static Animal createAnimal(int number, String name)
    {
        Animal newObject = new Animal();
        newObject.name = name;
        newObject.number = number;
        return newObject;
    }
    
}