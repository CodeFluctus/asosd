class HelloWorld {
    public static void main(String[] args) {
        System.out.println("ASD");
        HelloWorld helloOlio = new HelloWorld();

        //helloOlio.startApp();
        helloOlio.demonstrateThread();

    }

    private void demonstrateThread()
    {
        ExampleThread thread = new ExampleThread();
        thread.start();
    }

    private MammalType generateLehmaTyyppi()
    {
        MammalType lehmatyyppi = new MammalType();
        lehmatyyppi.name = "Lehmä";
        lehmatyyppi.latinName = "Vacca";
        lehmatyyppi.typeId = 54;
        return lehmatyyppi;
    }

    private void startApp()
    {
        System.out.println("Starting application! Please wait...");
        Animal seppo = Animal.createAnimal(123, "SepiSeppo");
        System.out.println(seppo.getName());

        Mammal lehma = new Mammal();
        lehma.setName("Mansikki");

        MammalType lehmaTyyppi = generateLehmaTyyppi();
        lehma.setMammalType(lehmaTyyppi);

        System.out.println("Shutting down application...");
    }
}