import java.time.*;
import java.time.temporal.ChronoUnit;

public class ExampleThread extends Thread
{

    @Override
    public void run()
    {
        try
        {
            while(true)
            {
                LocalDateTime now = LocalDateTime.now();
                LocalDateTime lectureEnds = LocalDateTime.of(2020, Month.JANUARY, 15, 19, 00);
                long diff = now.until(lectureEnds, ChronoUnit.SECONDS);
                System.out.println("Lecture ends in " + diff + " seconds.");
                sleep(2000);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}