package fi.example.app.thirdpartylibr;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.flaviofaria.kenburnsview.KenBurnsView;
import com.github.pedrovgs.lynx.LynxActivity;
import com.github.pedrovgs.lynx.LynxConfig;
import com.oguzdev.circularfloatingactionmenu.library.FloatingActionButton;
import com.oguzdev.circularfloatingactionmenu.library.FloatingActionMenu;
import com.oguzdev.circularfloatingactionmenu.library.SubActionButton;

public class MainActivity extends AppCompatActivity {

    KenBurnsView imageView;
    boolean introShowed = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        imageView = findViewById(R.id.imageView);

        if(!introShowed)
        {
            Intent introIntent = new Intent(this, IntroActivity.class);
            startActivity(introIntent);
            introShowed = true;
        }



        Glide.with(this).load("https://img.huffingtonpost.com/asset/5dcc613f1f00009304dee539.jpeg?cache=QaTFuOj2IM&ops=crop_834_777_4651_2994%2Cscalefit_720_noupscale").into(imageView);
        if(BuildConfig.DEBUG == true)
        {
            findViewById(R.id.lynxButton).setVisibility(View.VISIBLE);
            findViewById(R.id.lynxButton).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    openLynxActivity();
                }
            });
        }

        Log.d("OAMK", "Tämä näkyy lynxissä!");


        initFloatButton();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    private void initFloatButton() {
        ImageView icon = new ImageView(this); // Create an icon
        icon.setImageResource( R.drawable.button_action_touch);

        FloatingActionButton actionButton = new FloatingActionButton.Builder(this)
                .setContentView(icon)
                .build();


        //Creating sub-buttons
        SubActionButton.Builder itemBuilder = new SubActionButton.Builder(this);
        // repeat many times:
        ImageView itemIcon = new ImageView(this);
        itemIcon.setImageResource(R.drawable.button_action_dark);
        SubActionButton button1 = itemBuilder.setContentView(itemIcon).build();

        ImageView itemIcon2 = new ImageView(this);
        itemIcon.setImageResource(R.drawable.button_action_dark);
        SubActionButton button2 = itemBuilder.setContentView(itemIcon2).build();
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("ASD", "Sub-button clicked!");
            }
        });

        //Attaching sub-buttons
        FloatingActionMenu actionMenu = new FloatingActionMenu.Builder(this)
                .addSubActionView(button1)
                .addSubActionView(button2)
                .attachTo(actionButton)
                .build();
    }

    private void openLynxActivity() {
        LynxConfig lynxConfig = new LynxConfig();
        lynxConfig.setMaxNumberOfTracesToShow(4000);

        Intent lynxActivityIntent = LynxActivity.getIntent(this, lynxConfig);
        startActivity(lynxActivityIntent);
    }
}
