package fi.example.app.thirdpartylibr;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;

import com.hololo.tutorial.library.PermissionStep;
import com.hololo.tutorial.library.Step;
import com.hololo.tutorial.library.TutorialActivity;

public class IntroActivity extends TutorialActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addFragment(new Step.Builder().setTitle("Wellcome to OAMK")
                    .setContent("This is content text")
                    .setBackgroundColor(Color.CYAN)
                    .setDrawable(R.drawable.button_action_dark)
                    .setSummary("With this app you will be succesfull in school")
                    .build());

        addFragment(new Step.Builder().setTitle("See your lectures online")
                .setContent("This is content text")
                .setBackgroundColor(Color.MAGENTA)
                .setDrawable(R.drawable.button_action_dark)
                .setSummary("With this app you will be succesfull in school")
                .build());

        addFragment(new Step.Builder().setTitle("Do your thesis")
                .setContent("This is content text")
                .setBackgroundColor(Color.LTGRAY)
                .setDrawable(R.drawable.button_action_dark)
                .setSummary("With this app you will be succesfull in school")
                .build());
    }

    @Override
    public void currentFragmentPosition(int position) {

    }
}
