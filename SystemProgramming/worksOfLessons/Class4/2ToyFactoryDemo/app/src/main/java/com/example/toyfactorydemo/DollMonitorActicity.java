package com.example.toyfactorydemo;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

public class DollMonitorActicity extends AppCompatActivity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doll_monitor_acticity);
    }

    @Override
    protected void onResume() {
        super.onResume();
        //Register message receiver here
    }


    @Override
    protected void onStop() {
        super.onStop();

        //Unregister receiver here
    }


    public void toyReady(final String name) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                TextView textView = findViewById(R.id.dollMonitor);
                textView.append("Doll ready: " + name + "\n");
            }
        });
    }
}
