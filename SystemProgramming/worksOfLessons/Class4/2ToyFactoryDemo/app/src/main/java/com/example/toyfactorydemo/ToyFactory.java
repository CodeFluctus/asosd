package com.example.toyfactorydemo;

import android.content.Context;
import android.content.Intent;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

public class ToyFactory implements ToyMachine.MachineObserverInterface {
    ToyMachine teddyBearMachine = new ToyMachine(5000, "Teddy Bear", this);
    ToyMachine dollMachine = new ToyMachine(7000, "Doll", this);
    ToyMachine toyCarMachine = new ToyMachine(2000, "Toy Car", this);

    private Context context;

    public void setContext(Context context) {
        this.context = context;
    }

    private ArrayList<String> allManufacturedToys = new ArrayList<>();

    public ToyFactory() {
        teddyBearMachine.start();
        dollMachine.start();
        toyCarMachine.start();
    }

    @Override
    public void toyReady(String name) {
        allManufacturedToys.add(name);
        reportViaPublishAndSubscribe(name);
    }

    private void reportViaPublishAndSubscribe(String name)
    {
        Intent intent = new Intent(name);
        intent.putExtra("STATUS", "ready");
        intent.putExtra("MANUFACTTIME", new Date().toString());
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }
}
