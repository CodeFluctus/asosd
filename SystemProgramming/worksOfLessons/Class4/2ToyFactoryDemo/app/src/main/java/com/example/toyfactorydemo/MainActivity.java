package com.example.toyfactorydemo;

import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener
{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findViewById(R.id.startFactoryBtn).setOnClickListener(this);
        findViewById(R.id.monitorDollsBtn).setOnClickListener(this);

        LocalBroadcastManager.getInstance(this).registerReceiver(messageReceiver, new IntentFilter("Doll"));
    }

    @Override
    protected void onStop() {
        super.onStop();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(messageReceiver);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.startFactoryBtn) {

        }
        else if (view.getId() == R.id.monitorDollsBtn) {
            Intent intent = new Intent(this, DollMonitorActicity.class);
            startActivity(intent);
        }
    }

    private BroadcastReceiver messageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            TextView logView = findViewById(R.id.logTextView);
            logView.append("Toy Ready: " + intent.getAction() + "\n");
        }
    };

    /*public void toyReady(final String name) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                TextView logView = findViewById(R.id.logTextView);
                logView.append("Toy Ready: " + name + "\n");
            }
        });
    }*/
}
