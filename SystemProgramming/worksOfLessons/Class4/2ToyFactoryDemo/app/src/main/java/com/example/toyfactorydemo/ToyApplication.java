package com.example.toyfactorydemo;

import android.app.Application;

public class ToyApplication extends Application {

    ToyFactory toyFactory = new ToyFactory();

    @Override
    public void onCreate() {
        super.onCreate();
        toyFactory.setContext(this);
    }
}
