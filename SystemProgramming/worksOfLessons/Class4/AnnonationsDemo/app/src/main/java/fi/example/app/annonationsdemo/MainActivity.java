package fi.example.app.annonationsdemo;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

@EActivity(R.layout.activity_main)

public class MainActivity extends AppCompatActivity {

    @ViewById
    TextView helloText;

    @ViewById
    Button pressMeButton;

    @Click(R.id.pressMeButton)
    void pressMePressed()
    {
        Log.d("DEMO", "Press me button pressed");
        sleepInBackground();
    }

    @Background
    void sleepInBackground()
    {
        for (int i = 0; i < 10; i++) {
            try
            {
                Thread.sleep(2000);
                updateUi(i);
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
    }

    @UiThread
    public void updateUi(int i)
    {
        helloText.setText("Kierros " + i);
    }


}