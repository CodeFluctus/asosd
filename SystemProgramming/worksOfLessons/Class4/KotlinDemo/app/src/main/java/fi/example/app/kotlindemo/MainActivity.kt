package fi.example.app.kotlindemo

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import org.w3c.dom.Text

class MainActivity : AppCompatActivity() {

    open class Person(var name: String, var age: String)
    class Student(var number: Int, name: String, age: String) : Person(name, age);

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var Seppo = Person("Seppo Taalasmaa", "32")
        var opiskelija = Student(432,"Kalle Ismo", "42")

        findViewById<TextView>(R.id.textView).setOnClickListener {
            (it as TextView).text ="NappiaPainetaan"
        }

    }

    fun someOtherMethod(jokuBundle : Bundle)
    {

    }
}
