/**
 * OneTamagochi
 */
public class OneTamagochi extends Thread{

    private int foodAmmount = 10;
    private String name = null;
    private String[] namePool = {"Aki", "Ulla", "Ismo", "Kari", "Seppo", "Miia", "Kalle", "Saku", "Anni"};
    private boolean isAlive = true;

    OneTamagochi()
    {
        /*for(int i = 0; i < 100; i++)
        {*/
            double rand = Math.random();
            System.out.println("Rand: " + rand);
            System.out.println("NamePool: " + namePool.length);
            int nro = (int)(rand*(namePool.length));
            this.foodAmmount +=nro;
            this.name = namePool[nro];
            System.out.println(this.name);
        //}
        
    }

    @Override
    public void run() {
        // TODO Auto-generated method stub
        super.run();
        while(this.isAlive)
        {
            try 
            {
            
                this.foodAmmount -=1;
                if(this.foodAmmount == 0)
                {
                    this.isAlive = false;
                }
                sleep(2000);
                
            } catch (Exception e) 
            {
                //TODO: handle exception
                e.printStackTrace();
            }
        }
        
    }

    public void setTamagochiName(String newName)
    {
        this.name = newName;
    }

    public void feedTamagochi()
    {
        if(this.isAlive)
        {
            this.foodAmmount +=10;
            if(this.foodAmmount > 20)
            {
                this.isAlive = false;
            }
        }
    }

    public void printStatus()
    {
        if(this.isAlive)
        {
            System.out.println("Tamagochi " + this.name + " is happy and alive! Food left: " + this.foodAmmount);
        }
        else if(!this.isAlive && this.foodAmmount >= 20)
        {
            System.out.println("Tamagochi " + this.name + " got too fat and died to obiesity! Food left: " + this.foodAmmount);
        }
        else
        {
            System.out.println("Tamagochi " + this.name + " died to hunger! Food left: " + this.foodAmmount);
        }
    }

    public boolean isTamagochiAlive()
    {
        return this.isAlive;
    }

    public void killTamagochi()
    {
        this.isAlive = false;
    }


}