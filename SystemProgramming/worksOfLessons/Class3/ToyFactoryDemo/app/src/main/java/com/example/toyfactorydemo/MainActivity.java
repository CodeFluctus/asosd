package com.example.toyfactorydemo;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener,
        ToyFactory.FactoryObserver {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findViewById(R.id.startFactoryBtn).setOnClickListener(this);
        findViewById(R.id.monitorDollsBtn).setOnClickListener(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        ToyFactory.getInstance().removeObserver(this);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.startFactoryBtn) {
            ToyFactory.getInstance().addObserver("ALL", this);
        }
        else if (view.getId() == R.id.monitorDollsBtn) {
            Intent intent = new Intent(this, DollMonitorActicity.class);
            startActivity(intent);
        }
    }

    @Override
    public void toyReady(final String name) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                TextView logView = findViewById(R.id.logTextView);
                logView.append("Toy Ready: " + name + "\n");
            }
        });
    }
}
