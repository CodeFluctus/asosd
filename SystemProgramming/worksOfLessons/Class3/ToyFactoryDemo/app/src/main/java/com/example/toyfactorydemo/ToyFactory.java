package com.example.toyfactorydemo;

import java.util.ArrayList;
import java.util.HashMap;

public class ToyFactory implements ToyMachine.MachineObserverInterface {
    ToyMachine teddyBearMachine = new ToyMachine(5000, "Teddy Bear", this);
    ToyMachine dollMachine = new ToyMachine(7000, "Doll", this);
    ToyMachine toyCarMachine = new ToyMachine(2000, "Toy Car", this);

    private static ToyFactory singletonInstance = null;

    private ArrayList<String> allManufacturedToys = new ArrayList<>();

    public interface FactoryObserver {
        void toyReady(String name);
    }

    HashMap<String, FactoryObserver> observers = new HashMap<>();

    public void addObserver(String toyToObserve, FactoryObserver observer) {
        observers.put(toyToObserve, observer);
    }

    public void removeObserver(FactoryObserver observerToBeRemoved) {
        while (observers.values().remove(observerToBeRemoved));
    }

    public static ToyFactory getInstance() {
        if (singletonInstance == null) {
            singletonInstance = new ToyFactory();
        }
        return singletonInstance;
    }

    private ToyFactory() {
        teddyBearMachine.start();
        dollMachine.start();
        toyCarMachine.start();
    }

    @Override
    public void toyReady(String name) {
        allManufacturedToys.add(name);
        reportToObservers(name);
    }

    private void reportToObservers(String name) {
        FactoryObserver allObserver = observers.get("ALL");

        if (allObserver != null) {
            allObserver.toyReady(name);
        }

        FactoryObserver specific = observers.get(name);
        if (specific != null) {
            specific.toyReady(name);
        }
    }
}
