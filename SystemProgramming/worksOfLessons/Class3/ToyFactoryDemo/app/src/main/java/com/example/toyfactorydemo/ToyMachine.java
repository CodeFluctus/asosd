package com.example.toyfactorydemo;

public class ToyMachine extends Thread {

    public interface MachineObserverInterface {
        void toyReady(String name);
    }

    private int toyManufacturingTimeInMS = 5000;
    private String toyName = "Unknown";
    private MachineObserverInterface observer = null;

    private boolean running = true;

    public boolean isRunning() {
        return running;
    }

    public void setRunning(boolean running) {
        this.running = running;
    }

    ToyMachine(int manuTime, String name, MachineObserverInterface observer) {
        this.toyManufacturingTimeInMS = manuTime;
        this.toyName = name;
        this.observer = observer;
    }

    @Override
    public void run() {
        try {
            while (running) {
                sleep(toyManufacturingTimeInMS);
                observer.toyReady(toyName);
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
}
