package fi.example.app.xmlparser;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, XMLGetter.XMLGetterObserver, AdapterView.OnItemClickListener {

    private ListView hockeyLV;
    private HockeyMatchEngine hockeyMatchEngine;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findViewById(R.id.getXmlBtn).setOnClickListener(this);

        //Get's HockeyMatchEngine
        hockeyMatchEngine = HockeyMatchEngine.getInstance();
        //Clears engine so it won't show same match data's many times.
        hockeyMatchEngine.clearEngine();
        hockeyLV = findViewById(R.id.hockeyListView);
        hockeyLV.setOnItemClickListener(this);

    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.getXmlBtn)
        {
            XMLGetter xmlGetter = new XMLGetter();
            xmlGetter.setObserver(this);
            xmlGetter.execute();
        }
    }

    //Starts new Activity and sending matchindex as extra.
    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Log.d("ASD", "Index: " + i);
        Intent intent = new Intent(this, SingleMatchActivity.class);
        intent.putExtra("MATCHINDEX", i);
        startActivity(intent);
    }

    @Override
    public void InformationGathered() {

        /* Creates new ArrayList that stores HashMap including each match's data */
        ArrayList<HashMap<String, String>> aList = new ArrayList<HashMap<String, String>>();

        //Saves each match's data to HashMap and adding it to ArrayList
        for (int i = 0; i < hockeyMatchEngine.engingeSize(); i++) {
            HockeyMatch hockeyMatch = hockeyMatchEngine.giveMatchFromIndex(i);
            HashMap<String, String> hashMap = new HashMap<String, String>();
            hashMap.put("HomeTeam", hockeyMatch.getHomeTeam());
            hashMap.put("HomeGoals", Integer.toString(hockeyMatch.getHomeGoals()));
            hashMap.put("VisitorTeam", hockeyMatch.getVisitorTeam());
            hashMap.put("VisitorGoals", Integer.toString(hockeyMatch.getVisitorGoals()));
            aList.add(hashMap);
        }

        //Creating String array that includes keyvalues to HashMap
        String from[] = {"HomeTeam", "HomeGoals", "VisitorTeam", "VisitorGoals"};
        //Creating Int array that includes id's where to initialize each key's pointed values
        int to[] = {R.id.tvHomeTeam, R.id.tvHomeGoals, R.id.tvVisitorTeam, R.id.tvVisitorGoals};

        //Creating new SimpleAdapter and setting it to earlier initialized ListView.
        final SimpleAdapter adapter = new SimpleAdapter(getApplicationContext(), aList, R.layout.custom_row, from, to);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                hockeyLV.setAdapter(adapter);
            }
        });
    }
}
