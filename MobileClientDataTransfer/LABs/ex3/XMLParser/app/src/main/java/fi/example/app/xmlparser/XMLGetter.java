package fi.example.app.xmlparser;

import android.os.AsyncTask;
import android.util.Log;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

public class XMLGetter extends AsyncTask <Void, Integer, Boolean> {

    //ObserverInterface that will be used to communicate to other objects/activities
    private XMLGetterObserver observer = null;
    private String XMLInfo = null;

    //Interface and it's methods
    public interface XMLGetterObserver
    {
        void InformationGathered();
    }

    public void setObserver(XMLGetterObserver observer) {
        this.observer = observer;
    }

    @Override
    protected Boolean doInBackground(Void... voids) {

        //Current url where data will be fetched.
        //
        // !!!!! NEEDS VPN CONNECTION TO OAMK's network!!!!!
        //
        String urlString = "http://172.20.240.11:7002/";
        try {
            //Creating new URL-object and after that creating new HttpURLConnection.
            //Also opening connection to initialized url.
            URL url = new URL(urlString);
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();

            //Getting inputstream from url
            InputStream stream = httpURLConnection.getInputStream();

            //Converts inputstream to string.
            XMLInfo = convertInputStreamToString(stream);
            Log.d("ASD", XMLInfo);

            //Registers all matches from just created String to application's memory
            registerMatchesToApplication(XMLInfo);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Boolean aBoolean) {
        super.onPostExecute(aBoolean);
        //Informs observer that everything is ready and data is ready to show to user.
        observer.InformationGathered();

    }

    private void registerMatchesToApplication(String xml)
    {
        //Get HockeyMatchEngine.
        HockeyMatchEngine hockeyMatchEngine = HockeyMatchEngine.getInstance();
        try{
            //Create DocumentBuilderFactory and -Builder.
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();

            //Create StringBuilder and append it by method's parameter.
            StringBuilder xmlString = new StringBuilder();
            xmlString.append(xml);

            /* A ByteArrayInputStream contains an internal buffer that contains bytes that may be
            read from the stream. An internal counter keeps track
            of the next byte to be supplied by the read method.
            Source: https://docs.oracle.com/javase/7/docs/api/java/io/ByteArrayInputStream.html*/
            ByteArrayInputStream input = new ByteArrayInputStream(xmlString.toString().getBytes("UTF-8"));

            //DocumentBuilder object's method parses the content of given XML-document and returns
            // DOM Document-object
            Document doc = builder.parse(input);

            doc.getDocumentElement().normalize();

            //Get's first child node's name
            Log.d("ASD","Root element: " + doc.getDocumentElement().getNodeName());

            //Saves every child node by tag-name "match" to NodeList
            NodeList nList = doc.getElementsByTagName("match");

            Log.d("ASD","=======================================");

            //Iterates every Node from NodeList.
            for (int i = 0; i < nList.getLength(); i++)
            {

                Node nNode = nList.item(i);
                Log.d("ASD", "Current element: " + nNode.getNodeName());
                Element element = (Element) nNode;

                //Saves every element's first item's context to variable
                String homeTeam = element.getElementsByTagName("home_team").item(0).getTextContent();
                int homeGoals = Integer.parseInt(element.getElementsByTagName("home_goals").item(0).getTextContent());
                String visitorTeam = element.getElementsByTagName("visitor_team").item(0).getTextContent();
                int visitorGoals = Integer.parseInt(element.getElementsByTagName("visitor_goals").item(0).getTextContent());

                //Creating new HockeyMatch-object and add's it to HockeyMatchEngine.
                HockeyMatch hockeyMatch = new HockeyMatch(homeTeam, homeGoals, visitorTeam, visitorGoals);
                hockeyMatchEngine.addMatch(hockeyMatch);

                Log.d("ASD", "Home team: " + homeTeam);
                Log.d("ASD", "Visitor team: " + visitorTeam);
                Log.d("ASD", "Home goals: " + homeGoals);
                Log.d("ASD", "Visitor Goals: " + visitorGoals + "\n");
            }

            Log.d("ASD","=======================================");
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    private String convertInputStreamToString(InputStream inputStream)
    {
        String stringToReturn = "";
        try{
            /*Luetaan ensimmäinen merkki. Tällä saadaan myös käynnistymään available-metodi
             * Ilman tätä väittää saatavilla olevien merkkien määrän 0:si;
             * Koska read-metodi palauttaa ASCII-koodatun kokonaisluvun muutetaan se Character
             * olion avulla char-merkiksi ja tallennetaan stringiin.*/
            stringToReturn += Character.toString((char)inputStream.read());

            //Luetaan merkkejä Stringiin niin pitkään kuin merkkejä on saatavilla.
            while (inputStream.available() > 0)
            {
                stringToReturn += Character.toString((char)inputStream.read());
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        //Palautetaan luotu stringi.
        return stringToReturn;
    }
}
