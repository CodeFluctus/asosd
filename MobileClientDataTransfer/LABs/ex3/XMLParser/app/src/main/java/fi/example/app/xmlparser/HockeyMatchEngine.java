package fi.example.app.xmlparser;

import java.util.ArrayList;

public class HockeyMatchEngine {

    private ArrayList<HockeyMatch> allMatches = new ArrayList<>();
    private static HockeyMatchEngine singletonInstance = null;

    private HockeyMatchEngine(){

    }

    //HockeyMatchEngine is singleton, so it need to be initialized trough this method
    public static HockeyMatchEngine getInstance()
    {
        if(singletonInstance == null)
        {
            singletonInstance = new HockeyMatchEngine();
        }
        return singletonInstance;
    }

    public void addMatch(HockeyMatch hockeyMatch)
    {
        allMatches.add(hockeyMatch);
    }

    public ArrayList<HockeyMatch> giveEngine()
    {
        return allMatches;
    }

    public int engingeSize()
    {
        return allMatches.size();
    }

    public HockeyMatch giveMatchFromIndex(int i)
    {
        return allMatches.get(i);
    }

    public void clearEngine()
    {
        allMatches.clear();
    }


}
