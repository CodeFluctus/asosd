package fi.example.app.xmlparser;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

public class SingleMatchActivity extends AppCompatActivity {

    //Initializing textview objects
    TextView tvHomeTeam, tvVisitorTeam, tvHomeGoals, tvVisitorGoals;
    private int gameIndex = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_match);


        //Attaching created objects to objects from activity_main.xml
        tvHomeTeam = findViewById(R.id.tvHomeTeam);
        tvHomeGoals = findViewById(R.id.tvHomeGoals);
        tvVisitorTeam = findViewById(R.id.tvVisitorTeam);
        tvVisitorGoals = findViewById(R.id.tvVisitorGoals);

        /*SavedInstanceState is always null at first time Activity starts
          and it won't be initialized by overwriting onSavedInstanceState.
         */
        if(savedInstanceState == null)
        {
            //Get extras from intent
            Bundle extras = getIntent().getExtras();
            if(extras != null)
            {
                //Get selected matchindex from extras and show it's data to user
                gameIndex = extras.getInt("MATCHINDEX");
                HockeyMatchEngine hockeyMatchEngine = HockeyMatchEngine.getInstance();
                HockeyMatch hockeyMatch = hockeyMatchEngine.giveMatchFromIndex(gameIndex);

                tvHomeGoals.setText(Integer.toString(hockeyMatch.getHomeGoals()));
                tvHomeTeam.setText(hockeyMatch.getHomeTeam());
                tvVisitorGoals.setText(Integer.toString(hockeyMatch.getVisitorGoals()));
                tvVisitorTeam.setText(hockeyMatch.getVisitorTeam());
            }
        }


    }
}
