package fi.example.app.xmlparser;

public class HockeyMatch {

    private String homeTeam;
    private String visitorTeam;
    private int homeGoals;
    private int visitorGoals;

    public HockeyMatch(String homeTeam, int homeGoals, String visitorTeam, int visitorGoals)
    {
        this.homeTeam = homeTeam;
        this.homeGoals = homeGoals;
        this.visitorTeam = visitorTeam;
        this.visitorGoals = visitorGoals;
    }

    public String getHomeTeam() {
        return homeTeam;
    }

    public String getVisitorTeam() {
        return visitorTeam;
    }

    public int getHomeGoals() {
        return homeGoals;
    }

    public int getVisitorGoals() {
        return visitorGoals;
    }
}
