package fi.example.app.jsonparser;

import android.media.Image;

import java.util.HashMap;

public class AppContact {
    private String Id;
    private String name;
    private String email;
    private String address;
    private String gender;
    private int image;
    private HashMap<String, String> phone;

    AppContact(String id, String name, String email, String address, String gender)
    {
        this.Id = id;
        this.name = name;
        this.email = email;
        this.address = address;
        this.gender = gender;
        if(this.gender.equals("female"))
        {
            image = R.drawable.female;
        }
        else if(this.gender.equals("male"))
        {
            image = R.drawable.male;
        }
        else
        {
            image = R.drawable.ic_launcher_background;
        }
    }

    public int getImage() {
        return image;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public HashMap<String, String> getPhone() {
        return phone;
    }

    public void setPhone(HashMap<String, String> phone) {
        this.phone = phone;
    }
}
