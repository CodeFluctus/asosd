package fi.example.app.jsonparser;

import java.util.ArrayList;

public class AppContactsEngine {

    private ArrayList<AppContact> allAppContacts = new ArrayList<>();
    private static AppContactsEngine singletonInstance = null;

    private AppContactsEngine()
    {

    }

    public static AppContactsEngine getInstance()
    {
        if(singletonInstance == null)
        {
            singletonInstance = new AppContactsEngine();
        }
        return singletonInstance;
    }

    public ArrayList<AppContact> giveEngine()
    {
        return allAppContacts;
    }

    public int getEngineSize()
    {
        return allAppContacts.size();
    }

    public void addAppContact(AppContact contact)
    {
        allAppContacts.add(contact);
    }

    public AppContact giveMatchFromIndex(int i)
    {
        if(i >= 0 && i < allAppContacts.size())
        {
            return allAppContacts.get(i);
        }
        else
        {
            return null;
        }
    }

    public void clearEngine()
    {
        allAppContacts.clear();
    }
}
