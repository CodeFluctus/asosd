package fi.example.app.jsonparser;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import java.util.ArrayList;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity implements JSONGetter.JSONGetterObserver, View.OnClickListener {

    private ListView contactsLV;
    private AppContactsEngine appContactsEngine;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.downloadButton).setOnClickListener(this);

        //Gets AppContactsEngine
        appContactsEngine = AppContactsEngine.getInstance();

        contactsLV = findViewById(R.id.contactsListView);

    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.downloadButton)
        {
            JSONGetter jsonGetter = new JSONGetter();
            jsonGetter.setObserver(this);
            jsonGetter.execute();
        }
    }

    @Override
    public void InformationGathered() {

        ArrayList<HashMap<String,String>> arrayList = new ArrayList<>();

        for(int i = 0; i < appContactsEngine.getEngineSize(); i++)
        {
            AppContact contact = appContactsEngine.giveMatchFromIndex(i);

            HashMap<String, String> hashMap = new HashMap<>();
            hashMap.put("Name", contact.getName());
            hashMap.put("Address", contact.getAddress());
            hashMap.put("Email", contact.getEmail());
            hashMap.put("Image", Integer.toString(contact.getImage()));
            arrayList.add(hashMap);
        }

        //Creating String array that includes keyvalues to HashMap
        String from[] = {"Name" , "Address" , "Email", "Image"};

        //Creating Int array that includes id's where to initialize each key's pointed values
        int to[] = {R.id.personName, R.id.personAddress, R.id.personEmail, R.id.userImage};

        final SimpleAdapter adapter = new SimpleAdapter(this, arrayList, R.layout.custom_row, from, to);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                contactsLV.setAdapter(adapter);
            }
        });
    }
}
