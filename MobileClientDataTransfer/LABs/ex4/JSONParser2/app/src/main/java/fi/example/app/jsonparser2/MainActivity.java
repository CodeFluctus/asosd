package fi.example.app.jsonparser2;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import java.util.ArrayList;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity implements JSONGetter.JSONGetterObserver {

    private ListView conDataLV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        conDataLV = findViewById(R.id.conDataLV);

        JSONGetter jsonGetter = new JSONGetter();
        jsonGetter.setObserver(this);
        jsonGetter.execute();

    }

    @Override
    public void InformationGathered() {

        ConDataEngine conDataEngine = ConDataEngine.getInstance();
        ArrayList<HashMap<String,String>> arrayList = new ArrayList<>();

        for(int i = 0; i < conDataEngine.getEngineSize(); i++)
        {
            ConData conData = conDataEngine.getConDataFromIndex(i);

            HashMap<String, String> hashMap = new HashMap<>();
            hashMap.put("Name", conData.getName());
            hashMap.put("AddressAndPort", conData.getAddress() + ":" + conData.getPort());
            hashMap.put("Ip","IP: " + conData.getIp());
            arrayList.add(hashMap);
        }

        //Creating String array that includes keyvalues to HashMap
        String from[] = {"Name" , "AddressAndPort", "Ip"};

        //Creating Int array that includes id's where to initialize each key's pointed values
        int to[] = {R.id.conDataName, R.id.conDataAddressAndPort, R.id.conDataIp};

        final SimpleAdapter adapter = new SimpleAdapter(this, arrayList, R.layout.custom_row, from, to);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                conDataLV.setAdapter(adapter);
            }
        });
    }
}
