package fi.example.app.jsonparser2;

import java.util.ArrayList;

public class ConDataEngine {

    private ConDataEngine(){}
    private static ConDataEngine singleton = null;
    private ArrayList<ConData> allConDatas = new ArrayList<>();

    public static ConDataEngine getInstance()
    {
        if(singleton == null)
        {
            singleton = new ConDataEngine();
        }
        return singleton;
    }

    public int getEngineSize() {
        return allConDatas.size();
    }

    public void addConData(ConData conData)
    {
        allConDatas.add(conData);
    }

    public ConData getConDataFromIndex(int i)
    {
        if(i >= 0 && i < allConDatas.size())
        {
            return allConDatas.get(i);
        }
        return null;
    }
}
