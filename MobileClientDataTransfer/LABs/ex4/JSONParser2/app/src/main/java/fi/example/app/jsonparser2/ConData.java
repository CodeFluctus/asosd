package fi.example.app.jsonparser2;

public class ConData {
    private int id;
    private String name;
    private String address;
    private String ip;
    private String port;

    public ConData(int id, String name, String address, String ip, String port)
    {
        this.id = id;
        this.name = name;
        this.address = address;
        this.ip = ip;
        this.port = port;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }
}
