package fi.example.app.jsonparser2;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;

import javax.net.ssl.HttpsURLConnection;

public class JSONGetter extends AsyncTask<Void, Integer, Boolean> {

    //ObserverInterface that will be used to communicate to other objects/activities
    private JSONGetterObserver observer = null;
    private String JSONInfo = null;

    //Interface and it's methods
    public interface JSONGetterObserver
    {
        void InformationGathered();
    }

    public void setObserver(JSONGetterObserver observer) {
        this.observer = observer;
    }

    @Override
    protected Boolean doInBackground(Void... voids) {

        //Current url where data will be fetched.
        //
        // !!!!! NEEDS VPN CONNECTION TO OAMK's network!!!!!
        //
        String urlString = "http://172.20.240.11:7003";
        try {
            //Creating new URL-object and after that creating new HttpURLConnection.
            //Also opening connection to initialized url.
            URL url = new URL(urlString);
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();

            //Getting inputstream from url
            InputStream stream = httpURLConnection.getInputStream();

            //Converts inputstream to string.
            JSONInfo = convertInputStreamToString(stream);
            Log.d("ASD", JSONInfo);

            //Registers all matches from just created String to application's memory
            //registerMatchesToApplication(XMLInfo);

            saveConData(JSONInfo);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Boolean aBoolean) {
        super.onPostExecute(aBoolean);
        ConDataEngine conDataEngine = ConDataEngine.getInstance();
        Log.d("ASD", "Contacts count: " + conDataEngine.getEngineSize());
        observer.InformationGathered();
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
    }

    private String convertInputStreamToString(InputStream inputStream)
    {
        String stringToReturn = "";
        try{
            /*Luetaan ensimmäinen merkki. Tällä saadaan myös käynnistymään available-metodi
             * Ilman tätä väittää saatavilla olevien merkkien määrän 0:si;
             * Koska read-metodi palauttaa ASCII-koodatun kokonaisluvun muutetaan se Character
             * olion avulla char-merkiksi ja tallennetaan stringiin.*/
            stringToReturn += Character.toString((char)inputStream.read());

            //Luetaan merkkejä Stringiin niin pitkään kuin merkkejä on saatavilla.
            while (inputStream.available() > 0)
            {
                stringToReturn += Character.toString((char)inputStream.read());
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        //Palautetaan luotu stringi.
        return stringToReturn;
    }

    private void saveConData(String JsonString)
    {
        Log.d("ASD", "saveConData()START");
        ConDataEngine conDataEngine = ConDataEngine.getInstance();
        try
        {
            JSONObject mainObject = new JSONObject(JsonString);
            Log.d("ASD", mainObject.toString());
            JSONArray contactJSONArray = mainObject.getJSONArray("ports");
            for (int i = 0; i < contactJSONArray.length(); i++)
            {
                JSONObject singleObj = contactJSONArray.getJSONObject(i);
                int objId = singleObj.getInt("id");
                String objName = singleObj.getString("name");
                String objAddress = singleObj.getString("address");
                String objIp = singleObj.getString("ip");
                String objPort = singleObj.getString("port");

                /*JSONObject singleObjPhones = singleObj.getJSONObject("phone");
                String mobile = singleObjPhones.getString("mobile");
                String home = singleObjPhones.getString("home");
                String office = singleObjPhones.getString("office");
                HashMap<String, String> allPhones = new HashMap<>();
                allPhones.put("mobile", mobile);
                allPhones.put("home", home);
                allPhones.put("office", office);*/

                ConData conData = new ConData(objId, objName, objAddress, objIp, objPort);

                conDataEngine.addConData(conData);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }
}
