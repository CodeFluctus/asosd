package com.example.mcdtcallsmsemail;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.PermissionRequest;
import android.widget.Button;
import android.widget.Toast;

import java.util.HashMap;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    static final int SMS_REQUEST_CODE = 101;
    static final int CALL_REQUEST_CODE = 202;


    static final String[] PERMISSIONS = {Manifest.permission.CALL_PHONE, Manifest.permission.SEND_SMS};




    private Button buttonCall;
    private Button buttonSendSMS;
    private Button buttonSendEmail;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Initializing buttons and onClickListeners
        buttonCall = findViewById(R.id.buttonCall);
        buttonSendSMS = findViewById(R.id.buttonSendSMS);
        buttonSendEmail = findViewById(R.id.buttonSendEmail);

        buttonSendEmail.setOnClickListener(this);
        buttonSendSMS.setOnClickListener(this);
        buttonCall.setOnClickListener(this);


        //Request permissions for application
        ActivityCompat.requestPermissions(this, PERMISSIONS, 101);
    }

    @Override
    public void onClick(View v) {
        //If user denied earlier those permissions ask them again. In other case open activity
        if(v.getId() == R.id.buttonCall)
        {
            if(permissionGranted(Manifest.permission.CALL_PHONE))
            {
                openActivity(MakeCallActivity.class);
            }
            else
            {
                checkPermissionFor(Manifest.permission.CALL_PHONE, CALL_REQUEST_CODE);
            }

        }
        else if(v.getId() == R.id.buttonSendEmail)
        {
            openActivity(SendEmailActivity.class);
        }
        else if(v.getId() == R.id.buttonSendSMS)
        {
            if(permissionGranted(PERMISSIONS[1]))
            {
                openActivity(SendSMSActivity.class);
            }
            else
            {
                checkPermissionFor(PERMISSIONS[1], SMS_REQUEST_CODE);
            }
        }
    }


    //Checks if user have granted permission
    private boolean permissionGranted(String permission)
    {
            if(ContextCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED)
            {
                return false;
            }
        return true;
    }


    //Opens activity that is passed as parameter
    private void openActivity(Class classToOpen)
    {
        Intent intent = new Intent(this, classToOpen);
        startActivity(intent);

    }


    //Checks permission for wanted permission
    private void checkPermissionFor(String wantedPermissions, int permissionCode)
    {
        //If permission isn't already granted ask it
            if(ContextCompat.checkSelfPermission(this, wantedPermissions) != PackageManager.PERMISSION_GRANTED)
            {
                Log.d("ASD", "1. Ei oikeutta");
                if(ActivityCompat.shouldShowRequestPermissionRationale(this, wantedPermissions))
                {
                    Log.d("ASD", "2. Kysytään oikeutta");
                }
                //Invokes onRequestPermissionResult-method
                ActivityCompat.requestPermissions(this, new String[]{wantedPermissions}, permissionCode);
            }
            //Everything OK!
            else
            {
                Toast.makeText(this, "All permissions already granted!", Toast.LENGTH_SHORT).show();
            }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        //Read requestCode and show information to user about selected requestCode
        if(requestCode == SMS_REQUEST_CODE)
        {
            Log.d("ASD", "SMS PERMISSIONIA HAETAAN " + requestCode);
            if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
            {
                Log.d("ASD", "SMS OIKEUDET MYÖNNETTY!");
                Toast.makeText(this,"You can now send SMS", Toast.LENGTH_SHORT).show();
            }
            else
            {
                Toast.makeText(this,"You need to accept permission to send SMS!", Toast.LENGTH_SHORT).show();
                buttonSendSMS.setClickable(false);
                Log.d("ASD", "SMS OIKEUDET ESTETTY!");
            }
        }

        if(requestCode == CALL_REQUEST_CODE)
        {
            Log.d("ASD", "CALL PERMISSIONIA HAETAAN " + requestCode);
            if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
            {
                Log.d("ASD", "CALL OIKEUDET MYÖNNETTY!");
                Toast.makeText(this,"You can now make calls", Toast.LENGTH_SHORT).show();

            }
            else
            {
                Toast.makeText(this,"You need to accept permission to make calls!", Toast.LENGTH_SHORT).show();
                //buttonCall.setClickable(false);
                Log.d("ASD", "CALL OIKEUDET ESTETTY!");
            }
        }
    }
}
