package com.example.mcdtcallsmsemail;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.telecom.Call;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MakeCallActivity extends AppCompatActivity implements View.OnClickListener{

    private EditText editTextPhonenumber;
    private String LOG_TAG = "LOGGING 123";
    private boolean isPhoneCalling = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_make_call);
        findViewById(R.id.buttonCallNow).setOnClickListener(this);
        editTextPhonenumber = findViewById(R.id.editTextPhonenumber);

        //Initializes new PhoneStateListener and overwrites method named on CallStateChanged
        PhoneStateListener phoneStateListener = new PhoneStateListener(){
            @Override
            public void onCallStateChanged(int state, String phoneNumber) {
                super.onCallStateChanged(state, phoneNumber);
                if(TelephonyManager.CALL_STATE_RINGING == state)
                {
                    Log.d(LOG_TAG, "RINGING, number: " + phoneNumber);
                }
                if(TelephonyManager.CALL_STATE_OFFHOOK == state)
                {
                    Log.d(LOG_TAG, "OFFHOOK");
                    isPhoneCalling = true;
                }
                if(TelephonyManager.CALL_STATE_IDLE == state)
                {
                    Log.d(LOG_TAG, "IDLE");
                    if(isPhoneCalling)
                    {
                        Log.d(LOG_TAG, "Restart app");
                        Intent i = getBaseContext().getPackageManager().getLaunchIntentForPackage(getBaseContext().getPackageName());
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(i);
                        isPhoneCalling = false;
                    }
                }
            }
        };
        TelephonyManager telephonyManager = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
        telephonyManager.listen(phoneStateListener, PhoneStateListener.LISTEN_CALL_STATE);

    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.buttonCallNow)
        {
            //Reads user input from editText
            String phonenumber = editTextPhonenumber.getText().toString();
            call("tel:"+phonenumber);
        }
    }

    protected void call(String callData) {
        //Initializes intent for call and starts it
        Intent callIntent = new Intent(Intent.ACTION_DIAL); //dial
        callIntent.setData(Uri.parse(callData));
//Intent in=new Intent(Intent.ACTION_CALL,Uri.parse("0401415086"));
        try{
            startActivity(callIntent);
        }
        catch (android.content.ActivityNotFoundException ex){
            Toast.makeText(getApplicationContext(),"yourActivity is not founded", Toast.LENGTH_SHORT).show();
        }
    }
}
