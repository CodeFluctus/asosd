package com.example.mcdtcallsmsemail;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class SendEmailActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText etEmailTo, etEmailSubject, etEmailMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_email);
        findViewById(R.id.buttonSendEmail).setOnClickListener(this);

        etEmailMessage = findViewById(R.id.editTextEmailMessage);
        etEmailTo = findViewById(R.id.editTextEmail);
        etEmailSubject = findViewById(R.id.editTextSubj);

    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.buttonSendEmail)
        {
            String to = etEmailTo.getText().toString();
            String subject = etEmailSubject.getText().toString();
            String message = etEmailMessage.getText().toString();

            //Initializes intent for email and put required extras for subject,message and email-address
            Intent email = new Intent(Intent.ACTION_SEND);
            email.putExtra(Intent.EXTRA_EMAIL, new String[]{to});
            email.putExtra(Intent.EXTRA_SUBJECT, subject);
            email.putExtra(Intent.EXTRA_TEXT, message);

            //Tells application to use only email-clients. Otherwise won't recognize what is wanted.
            email.setType("message/rrc822");

            startActivity(Intent.createChooser(email, "Choose an Email client!"));
        }
    }
}
