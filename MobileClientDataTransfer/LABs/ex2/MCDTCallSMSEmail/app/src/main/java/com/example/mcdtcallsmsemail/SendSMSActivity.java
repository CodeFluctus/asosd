package com.example.mcdtcallsmsemail;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.telephony.SmsManager;
import android.view.View;
import android.widget.EditText;

public class SendSMSActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText editTextNumber, editTextMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_sms);

        editTextNumber = findViewById(R.id.editTextNumber);
        editTextMessage = findViewById(R.id.editTextMessage);
        findViewById(R.id.button).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.button)
        {
            //Initializes new SmsManager API
            SmsManager smsManager = SmsManager.getDefault();
            //Reads number and message from textEdits
            String number = editTextNumber.getText().toString();
            String message = editTextMessage.getText().toString();
            //Sends sms-message to desired number
            smsManager.sendTextMessage(number, null, message, null, null);
        }
    }
}
