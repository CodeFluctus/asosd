package fi.example.app.mcdtlistonandr;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemClickListener {


    //Declaring private variables and objects
    private ListView listView;
    private ArrayList<String> carList = new ArrayList<>();
    private ArrayAdapter<String> aa;
    private int indexOfCar;
    private EditText editText;
    private boolean removebuttonPressed = false, editbuttonPressed = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Initializing declared objects
        //Initializing OnClickListeners for buttons and listview
        aa  = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, carList);
        listView = findViewById(R.id.listView);
        listView.setOnItemClickListener(this);
        editText = findViewById(R.id.editText);

        findViewById(R.id.buttonAdd).setOnClickListener(this);
        findViewById(R.id.button2ndActivity).setOnClickListener(this);
        findViewById(R.id.buttonEdit).setOnClickListener(this);
        findViewById(R.id.buttonRemove).setOnClickListener(this);

        listView.setAdapter(aa);

        //Adding new cars to arraylist
        carList.add("Toyta");
        carList.add("BMW");
        carList.add("Nissan");
        carList.add("Volvo");
        carList.add("Skoda");
    }

    @Override
    public void onClick(View view) {
        //Button starts new activity
        if (view.getId() == R.id.button2ndActivity)
        {
            Intent intent = new Intent(this, SecondActivity.class);
            startActivity(intent);
        }
        else if (view.getId() == R.id.buttonAdd)
        {
            //If user is already editing listView give hint to complete that first.
            if(editbuttonPressed) {
                Toast.makeText(this, "You are editing line! Save edit by clicking edit-button. Then try again", Toast.LENGTH_LONG).show();
            }
            else
            {
                String carToAdd;
                //If text at editText is null or empty advice user to put correct input.
                if(editText.getText().toString() == null || editText.getText().toString().matches(""))
                {
                    Toast.makeText(this,"Wrong format!",Toast.LENGTH_SHORT).show();
                    Log.d("ASD", "Test");
                }
                else
                {
                    //Read userinput from editText and add it to listView. After that clear editText.
                    carToAdd = editText.getText().toString();
                    carList.add(carToAdd);
                    Toast.makeText(this,"Car " + carToAdd + " added to list!",Toast.LENGTH_SHORT).show();
                    editText.setText("");
                }
            }
            //Tell to program by using editText method onEditorAction that text editing is done and program may close keyboard.
            editText.onEditorAction(EditorInfo.IME_ACTION_DONE);

        }
        else if (view.getId() == R.id.buttonRemove)
        {
            //If user is already editing listView give hint to complete that first.
            if(editbuttonPressed)
            {
                Toast.makeText(this, "You are editing line! Save edit by clicking edit-button. Then try again", Toast.LENGTH_LONG).show();
            }
            else
            {
                //Show next advice to user.
                Toast.makeText(this, "Click car name from list to remove it.", Toast.LENGTH_SHORT).show();
                removebuttonPressed = true;
            }
        }
        else if (view.getId() == R.id.buttonEdit)
        {
            if(editbuttonPressed == false)
            {
                Toast.makeText(this, "Click car name from list to edit it.", Toast.LENGTH_SHORT).show();
                editbuttonPressed = true;
            }
            else if(editbuttonPressed == true)
            {
                String editedCar = editText.getText().toString();
                carList.set(indexOfCar, editedCar);
                editbuttonPressed = false;
                editText.setText("");
                editText.onEditorAction(EditorInfo.IME_ACTION_DONE);
            }

        }
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Log.d("ASD", "onClick!");
        //If user had lastly pressed removeButton it will remove item from selected index
        if(removebuttonPressed)
        {
            Log.d("ASD", "Removing item number: " + i);
            carList.remove(i);
            removebuttonPressed = false;
            //Tell to ArrayAdapter that data is changed in listView.
            aa.notifyDataSetChanged();
        }
        //If user had lastly pressed editButton it will pick item name to editText.
        else if(editbuttonPressed)
        {
            String carName = carList.get(i);
            indexOfCar = i;
            Log.d("ASD", carName);
            editText.setText(carName);
        }
    }
}
