package fi.example.app.mcdtlistonandr;

import java.util.ArrayList;

public class CarList {

    private ArrayList<String> carList = new ArrayList<>();

    public void addCarToList(String car)
    {
        carList.add(car);
    }

    public String giveCarOfIndex(int i)
    {
        if(i < carList.size() && i > -1)
        {
            return carList.get(i);
        }
        else
        {
            return null;
        }
    }


}
