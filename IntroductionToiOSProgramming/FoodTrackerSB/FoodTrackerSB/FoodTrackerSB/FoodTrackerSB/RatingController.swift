//
//  RatingController.swift
//  FoodTrackerSB
//
//  Created by vind-opisk on 1.2.2020.
//  Copyright © 2020 1. All rights reserved.
//

import UIKit

@IBDesignable class RatingController: UIStackView {
    
    //MARK: Properties
    @IBInspectable var starSize : CGSize = CGSize(width: 44.0, height: 44.0)
    {
        didSet
        {
            setupButtons()
        }
    }
    @IBInspectable var starCount : Int = 5
    {
        didSet
        {
            setupButtons()
        }
    }
    private var ratingButtons = [UIButton]()
    var rating = 0
    {
        didSet
        {
            updateButtonSelectionStates()
        }
    }

    //MARK: Initialization
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupButtons()
    }
    
    required init(coder: NSCoder) {
        super.init(coder: coder)
        setupButtons()
    }
    
    //MARK: Private Methods
    
    private func setupButtons()
    {
        // clear any existing buttons
        for button in ratingButtons {
            removeArrangedSubview(button)
            button.removeFromSuperview()
        }
        ratingButtons.removeAll()
        
        //Load button images
        let bundle = Bundle(for: type(of: self))
        let filledStar = UIImage(named: "filledStar", in: bundle, compatibleWith: self.traitCollection)
        let emptyStar = UIImage(named: "emptyStar", in: bundle, compatibleWith: self.traitCollection)
        let highlighedStar = UIImage(named: "highlightedStar", in: bundle, compatibleWith: self.traitCollection)
        
        
        for index in 0..<starCount
        {
            //Create button
            let button = UIButton()
            
            //Set the button images
            button.setImage(emptyStar, for: .normal)
            button.setImage(filledStar, for: .selected)
            button.setImage(highlighedStar, for: .highlighted)
            button.setImage(highlighedStar, for: [.highlighted, .selected])
            
            //Add constraints
            button.translatesAutoresizingMaskIntoConstraints = false
            button.heightAnchor.constraint(equalToConstant: starSize.height).isActive = true
            button.widthAnchor.constraint(equalToConstant: starSize.width).isActive = true
            
            //Set te accessibility label
            button.accessibilityLabel = "Set \(index + 1) star rating"
            //Set button action
            button.addTarget(self, action: #selector(RatingController.ratingButtonTapped(button:)), for: .touchUpInside)
            
            //Add button to stack
            addArrangedSubview(button)
            // Add the new button to the rating button array
            ratingButtons.append(button)
        }
        
        updateButtonSelectionStates()
    }
    
    //MARK: Button Action
    @objc func ratingButtonTapped(button : UIButton)
    {
        //print("Button pressed!")
        guard let index = ratingButtons.index(of: button)
        else
        {
            fatalError("The button \(button), is not in the ratingButtons array: \(ratingButtons)")
        }
        
        let selectedrating = index + 1
        
        if(selectedrating == rating)
        {
            //If selected star represents the current rating, reset the rating to 0.
            rating = 0
        }
        else
        {
            rating = selectedrating
        }
    }
    
    private func updateButtonSelectionStates()
    {
        for(index, button) in ratingButtons.enumerated()
        {
            // If the index of a button is less than the rating, that button should be selected.
            button.isSelected = index < rating
            
            // Set the hint string for the currently selected star
            let hintString : String?
            if(rating == index + 1)
            {
                hintString = "Tap to reset the rating to zero."
            }
            else
            {
                hintString = nil
            }
            
            // Calculate the value string
            let valueString : String
            switch (rating) {
            case 0:
                valueString = "No rating set."
            case 1:
                valueString = "1 star set."
            default:
                valueString = "\(rating) stars set."
            }
            button.accessibilityHint = hintString
            button.accessibilityValue = valueString
        }
    }

}
