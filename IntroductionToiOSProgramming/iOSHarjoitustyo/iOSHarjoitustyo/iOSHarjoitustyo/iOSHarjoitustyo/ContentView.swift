//
//  ContentView.swift
//  iOSHarjoitustyo
//
//  Created by vind-opisk on 24.1.2020.
//  Copyright © 2020 1. All rights reserved.
//

import SwiftUI

struct allButtonStyle: ButtonStyle
{
    func makeBody( configuration: Configuration) -> some View
    {
        configuration.label
        .padding(25)
            .background(Color.gray)
        .frame(width: 60, height: 60)
            .border(Color.blue, width: 2)
        .cornerRadius(90)
        
    }
}

func addNumbers(_ numberOne : Float, _ numberTwo : Float) -> Float
{
    let numberToReturn : Float = Float(numberOne) + Float(numberTwo)
    return numberToReturn
}

func subtractNumbers(_ numberOne : Float, _ numberTwo : Float) -> Float
{
    let numberToReturn : Float = Float(numberOne) - Float(numberTwo)
    return numberToReturn
}

func multiplyNumbers(_ numberOne : Float, _ numberTwo : Float) -> Float
{
    let numberToReturn : Float = Float(numberOne) * Float(numberTwo)
    return numberToReturn
}

func divideNumbers(_ numberOne : Float, _ numberTwo : Float) -> Float
{
    let numberToReturn : Float = Float(numberOne) / Float(numberTwo)
    return numberToReturn
}
/*
func addNumbers(_ numberOne : Float, _ numberTwo : Float) -> Float
{
    let numberToReturn : Float = numberOne + numberTwo
    return numberToReturn
}*/



struct ContentView: View {
    
    @State var calculatorIncome : Float = 0
    @State var firstNumber : Float = 0
    @State var secondNumber : Float = 0
    @State var selectedOperator : String = " "
    @State var dotSelected : Bool = false
    @State var decimalCounter = 1.0;
    @State var dotButtonColor = Color.white
    
    var body: some View {
        ZStack
        {
            
            
            VStack(spacing: 30)
                {
                    HStack
                    {
                        Text(" ")
                        Text(String(firstNumber)).font(.largeTitle)
                    }
                    HStack
                    {
                        Text(String(selectedOperator)).font(.largeTitle)
                        Text(String(secondNumber)).font(.largeTitle)
                    }
                    HStack
                    {
                        Text(" ")
                        Text(String(calculatorIncome)).font(.largeTitle)
                    }
                HStack(spacing: 30)
                {
                    Button(action: {
                        if(self.calculatorIncome != 0)
                        {
                            self.clearInputs()
                            self.firstNumber = self.firstNumber * 10 + 1
                        }
                        else
                        {
                            if(self.selectedOperator == " ")
                            {
                                if(!self.dotSelected)
                                {
                                    self.decimalCounter = 1
                                    self.firstNumber = self.firstNumber * 10 + 1
                                }
                                else
                                {
                                    self.firstNumber = self.firstNumber + 1 / Float(pow(10.0,self.decimalCounter))
                                    self.decimalCounter = self.decimalCounter + 1
                                }
                            }
                            else
                            {
                                if(!self.dotSelected)
                                {
                                    self.decimalCounter = 1
                                    self.secondNumber = self.secondNumber * 10 + 1
                                }
                                else
                                {
                                    self.secondNumber = self.secondNumber + 1 / Float(pow(10.0,self.decimalCounter))
                                    self.decimalCounter = self.decimalCounter + 1
                                }
                            }
                        }
                    },
                           label:
                        {
                            Text("1")
                        }).buttonStyle(allButtonStyle())
                    Button(action: {
                        if(self.calculatorIncome != 0)
                        {
                            self.clearInputs()
                            self.firstNumber = self.firstNumber * 10 + 2
                        }
                        else
                        {
                            if(self.selectedOperator == " ")
                            {
                                if(!self.dotSelected)
                                {
                                    self.decimalCounter = 1
                                    self.firstNumber = self.firstNumber * 10 + 2
                                }
                                else
                                {
                                    self.firstNumber = self.firstNumber + 2 / Float(pow(10.0,self.decimalCounter))
                                    self.decimalCounter = self.decimalCounter + 1
                                }
                            }
                            else
                            {
                                if(!self.dotSelected)
                                {
                                    self.decimalCounter = 1
                                    self.secondNumber = self.secondNumber * 10 + 2
                                }
                                else
                                {
                                    self.secondNumber = self.secondNumber + 2 / Float(pow(10.0,self.decimalCounter))
                                    self.decimalCounter = self.decimalCounter + 1
                                }
                            }
                        }
                        },label:
                        {
                            Text("2")
                        }).buttonStyle(allButtonStyle())
                    Button(action: {
                        if(self.calculatorIncome != 0)
                        {
                            self.clearInputs()

                            self.firstNumber = self.firstNumber * 10 + 3
                        }
                        else
                        {
                            if(self.selectedOperator == " ")
                            {
                                if(!self.dotSelected)
                                {
                                    self.decimalCounter = 1
                                    self.firstNumber = self.firstNumber * 10 + 3
                                }
                                else
                                {
                                    self.firstNumber = self.firstNumber + 3 / ( Float(pow(10.0,self.decimalCounter)))
                                    self.decimalCounter = self.decimalCounter + 1
                                }
                            }
                            else
                            {
                                if(!self.dotSelected)
                                {
                                    self.decimalCounter = 1
                                    self.secondNumber = self.secondNumber * 10 + 3
                                }
                                else
                                {
                                    self.secondNumber = self.secondNumber + 3 / Float(pow(10.0,self.decimalCounter))
                                    self.decimalCounter = self.decimalCounter + 1
                                }
                            }
                        }
                        },label:
                        {
                            Text("3")
                        }).buttonStyle(allButtonStyle())
                    Button(action: {self.selectedOperator = "+"
                        self.dotSelected = false
                        if(self.calculatorIncome != 0)
                        {
                            let memory = self.calculatorIncome
                            self.clearInputs()
                            self.selectedOperator = "+"
                            self.firstNumber = memory
                        }
                    },
                       label:
                    {
                        Text("+")
                    }).buttonStyle(allButtonStyle())
                }
                HStack(spacing: 30)
                {
                    Button(action: {
                        if(self.calculatorIncome != 0)
                        {
                            self.clearInputs()
                            self.firstNumber = self.firstNumber * 10 + 4
                        }
                        else
                        {
                            if(self.selectedOperator == " ")
                            {
                                if(!self.dotSelected)
                                {
                                    self.decimalCounter = 1
                                    self.firstNumber = self.firstNumber * 10 + 4
                                }
                                else
                                {
                                    self.firstNumber = self.firstNumber + 4 / Float(pow(10.0,self.decimalCounter))
                                    self.decimalCounter = self.decimalCounter + 1
                                }
                            }
                            else
                            {
                                if(!self.dotSelected)
                                {
                                    self.decimalCounter = 1
                                    self.secondNumber = self.secondNumber * 10 + 4
                                }
                                else
                                {
                                    self.secondNumber = self.secondNumber + 4 / Float(pow(10.0,self.decimalCounter))
                                    self.decimalCounter = self.decimalCounter + 1
                                }
                            }
                        }
                        },label:
                        {
                            Text("4")
                        }).buttonStyle(allButtonStyle())
                    Button(action: {
                        if(self.calculatorIncome != 0)
                        {
                            self.clearInputs()
                            self.firstNumber = self.firstNumber * 10 + 5
                        }
                        else
                        {
                            if(self.selectedOperator == " ")
                            {
                                if(!self.dotSelected)
                                {
                                    self.decimalCounter = 1
                                    self.firstNumber = self.firstNumber * 10 + 5
                                }
                                else
                                {
                                    self.firstNumber = self.firstNumber + 5 / Float(pow(10.0,self.decimalCounter))
                                    self.decimalCounter = self.decimalCounter + 1
                                }
                            }
                            else
                            {
                                if(!self.dotSelected)
                                {
                                    self.decimalCounter = 1
                                    self.secondNumber = self.secondNumber * 10 + 5
                                }
                                else
                                {
                                    self.secondNumber = self.secondNumber + 5 / Float(pow(10.0,self.decimalCounter))
                                    self.decimalCounter = self.decimalCounter + 1
                                }
                            }
                        }
                        },label:
                        {
                            Text("5")
                        }).buttonStyle(allButtonStyle())
                    Button(action: {
                        if(self.calculatorIncome != 0)
                        {
                            self.clearInputs()
                            self.firstNumber = self.firstNumber * 10 + 6
                        }
                        else
                        {
                            if(self.selectedOperator == " ")
                            {
                                if(!self.dotSelected)
                                {
                                    self.decimalCounter = 1
                                    self.firstNumber = self.firstNumber * 10 + 6
                                }
                                else
                                {
                                    self.firstNumber = self.firstNumber + 6 / Float(pow(10.0,self.decimalCounter))
                                    self.decimalCounter = self.decimalCounter + 1
                                }
                            }
                            else
                            {
                                if(!self.dotSelected)
                                {
                                    self.decimalCounter = 1
                                    self.secondNumber = self.secondNumber * 10 + 6
                                }
                                else
                                {
                                    self.secondNumber = self.secondNumber + 6 / Float(pow(10.0,self.decimalCounter))
                                    self.decimalCounter = self.decimalCounter + 1
                                }
                            }
                        }
                        },label:
                        {
                            Text("6")
                        }).buttonStyle(allButtonStyle())
                    Button(action: {self.selectedOperator = "-"
                        self.dotSelected = false
                        if(self.calculatorIncome != 0)
                        {
                            let memory = self.calculatorIncome
                            self.clearInputs()
                            self.selectedOperator = "-"
                            self.firstNumber = memory
                        }
                    },
                       label:
                    {
                        Text("-")
                    }).buttonStyle(allButtonStyle())
                }
                HStack(spacing: 30)
                {
                    Button(action: {
                        if(self.calculatorIncome != 0)
                        {
                            self.clearInputs()
                            self.firstNumber = self.firstNumber * 10 + 7
                        }
                        else
                        {
                            if(self.selectedOperator == " ")
                            {
                                if(!self.dotSelected)
                                {
                                    self.decimalCounter = 1
                                    self.firstNumber = self.firstNumber * 10 + 7
                                }
                                else
                                {
                                    self.firstNumber = self.firstNumber + 7 / Float(pow(10.0,self.decimalCounter))
                                    self.decimalCounter = self.decimalCounter + 1
                                }
                            }
                            else
                            {
                                if(!self.dotSelected)
                                {
                                    self.decimalCounter = 1
                                    self.secondNumber = self.secondNumber * 10 + 7
                                }
                                else
                                {
                                    self.secondNumber = self.secondNumber + 7 / Float(pow(10.0,self.decimalCounter))
                                    self.decimalCounter = self.decimalCounter + 1
                                }
                            }
                        }
                        },label:
                        {
                            Text("7")
                        }).buttonStyle(allButtonStyle())
                    Button(action: {
                        if(self.calculatorIncome != 0)
                        {
                            self.clearInputs()
                            self.firstNumber = self.firstNumber * 10 + 8
                        }
                        else
                        {
                            if(self.selectedOperator == " ")
                            {
                                if(!self.dotSelected)
                                {
                                    self.decimalCounter = 1
                                    self.firstNumber = self.firstNumber * 10 + 8
                                }
                                else
                                {
                                    self.firstNumber = self.firstNumber + 8 / Float(pow(10.0,self.decimalCounter))
                                    self.decimalCounter = self.decimalCounter + 1
                                }
                            }
                            else
                            {
                                if(!self.dotSelected)
                                {
                                    self.decimalCounter = 1
                                    self.secondNumber = self.secondNumber * 10 + 8
                                }
                                else
                                {
                                    self.secondNumber = self.secondNumber + 8 / Float(pow(10.0,self.decimalCounter))
                                    self.decimalCounter = self.decimalCounter + 1
                                }
                            }
                        }
                        },label:
                        {
                            Text("8")
                        }).buttonStyle(allButtonStyle())
                    Button(action: {
                        if(self.calculatorIncome != 0)
                        {
                            self.clearInputs()
                            self.firstNumber = self.firstNumber * 10 + 9
                        }
                        else
                        {
                            if(self.selectedOperator == " ")
                            {
                                if(!self.dotSelected)
                                {
                                    self.decimalCounter = 1
                                    self.firstNumber = self.firstNumber * 10 + 9
                                }
                                else
                                {
                                    self.firstNumber = self.firstNumber + 9 / Float(pow(10.0,self.decimalCounter))
                                    self.decimalCounter = self.decimalCounter + 1
                                }
                            }
                            else
                            {
                                if(!self.dotSelected)
                                {
                                    self.decimalCounter = 1
                                    self.secondNumber = self.secondNumber * 10 + 9
                                }
                                else
                                {
                                    self.secondNumber = self.secondNumber + 9 / Float(pow(10.0,self.decimalCounter))
                                    self.decimalCounter = self.decimalCounter + 1
                                }
                            }
                        }
                        },label:
                        {
                            Text("9")
                        }).buttonStyle(allButtonStyle())
                    Button(action: {self.selectedOperator = "*"
                        self.dotSelected = false
                        if(self.calculatorIncome != 0)
                        {
                            let memory = self.calculatorIncome
                            self.clearInputs()
                            self.selectedOperator = "*"
                            self.firstNumber = memory
                        }
                    },
                       label:
                    {
                        Text("*")
                    }).buttonStyle(allButtonStyle())
                }
                HStack (spacing : 30)
                {
                    Button(action: {
                        self.dotSelected = !self.dotSelected
                        if(self.dotSelected)
                        {
                            self.dotButtonColor = Color.gray
                        }
                        else
                        {
                            self.dotButtonColor = Color.white
                        }
                        
                    },
                       label:
                    {
                        Text(".")
                    }).buttonStyle(allButtonStyle())
                        .padding(25)
                        .background(self.dotButtonColor)
                    HStack(spacing: 30)
                    {
                        Button(action: {
                            if(self.calculatorIncome != 0)
                            {
                                self.clearInputs()
                            }
                            if(self.selectedOperator == " ")
                            {
                                self.firstNumber = self.firstNumber * 10
                            }
                            else
                            {
                                self.secondNumber = self.secondNumber * 10
                            }
                            },label:
                            {
                                Text("0")
                            }).buttonStyle(allButtonStyle())
                    }
                    Button(action: {
                        self.dotSelected = false
                        if(self.selectedOperator == "+")
                        {
                            self.calculatorIncome = addNumbers(self.firstNumber, self.secondNumber)
                        }
                        else if(self.selectedOperator == "-")
                        {
                            self.calculatorIncome = subtractNumbers(self.firstNumber, self.secondNumber)
                        }
                        else if(self.selectedOperator == "*")
                        {
                            self.calculatorIncome = multiplyNumbers(self.firstNumber, self.secondNumber)
                        }
                        else if(self.selectedOperator == "/")
                        {
                            self.calculatorIncome = divideNumbers(self.firstNumber, self.secondNumber)
                        }
                    },
                       label:
                    {
                        Text("=")
                    }).buttonStyle(allButtonStyle())
                    
                    Button(action: {self.selectedOperator = "/"
                        self.dotSelected = false
                        if(self.calculatorIncome != 0)
                        {
                            let memory = self.calculatorIncome
                            self.clearInputs()
                            self.selectedOperator = "/"
                            self.firstNumber = memory
                        }
                    },
                       label:
                    {
                        Text("/")
                    }).buttonStyle(allButtonStyle())
                }
                    Button(action: {self.clearInputs()
                    },
                       label:
                    {
                        Text("C")
                    }).buttonStyle(allButtonStyle())
            }
        }
        
        
    }
    
    func clearInputs()
    {
        self.calculatorIncome = 0
        self.firstNumber = 0
        self.secondNumber = 0
        self.selectedOperator = " "
        self.dotSelected = false
        self.decimalCounter = 1
        
    }
}



struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
